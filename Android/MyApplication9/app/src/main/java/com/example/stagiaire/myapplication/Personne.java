package com.example.stagiaire.myapplication;

/**
 * Created by Administrateur on 04/09/2017.
 */

public class Personne {
    private String _nom;
    private String _prenom;
    private int _age;

    public Personne(String prenom, String nom, int age) {
        _nom = nom;
        _prenom = prenom;
        _age = age;
    }


    public String getNom() {
        return _nom;
    }

    public void setNom(String _nom) {
        this._nom = _nom;
    }

    public String getPrenom() {
        return _prenom;
    }

    public void setPrenom(String _prenom) {
        this._prenom = _prenom;
    }

    public int getAge() {
        return _age;
    }

    public void setAge(int _age) {
        this._age = _age;
    }
}
