package com.example.stagiaire.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button ok;
    TextView compteur;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Remonter les controls
        ok = (Button) findViewById(R.id.ok);
        ok.setText("Demarrer");
        compteur = (TextView) findViewById(R.id.compteur);
        // gerer le click du bouton
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ActivitySendXML.class);
                startActivity(i);

                /*Intent intent = new Intent(MainActivity.this, MyService.class);
                // Mettre en place un receiver
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(MyService.MON_ACTION);
                registerReceiver(new MonReceiver(), intentFilter);
                // Envoyer la valeur initiale du compteur
                Bundle b=new Bundle();
                b.putInt("compteur", 0);
                intent.putExtras(b);
                startService(intent);*/
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    // classe interne pour les echanges avec le service
    private class MonReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            int compteur = arg1.getIntExtra("compteur", 0);
            Toast.makeText(MainActivity.this,
                    "Depuis le service  !\n"
                            + " compteur: " + String.valueOf(compteur),
                    Toast.LENGTH_LONG).show();
        }
    }
}