package com.example.stagiaire.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.IOException;

public class ActivitySendXML extends AppCompatActivity {

    private Button btnSendXML;
    private EditText edtPrenom;
    private EditText edtNom;
    private EditText edtAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_xml);
        btnSendXML = (Button)findViewById(R.id.btnSendXML);
        btnSendXML.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Personne p = new Personne(edtPrenom.getText().toString(), edtNom.getText().toString(), Integer.parseInt(edtAge.getText().toString()));



                Serializer serializer = new Persister();
                Log.i("DIR = ", getApplicationInfo().dataDir);
                File file = new File(getApplicationInfo().dataDir + "/personne.xml");
                if(!file.exists())
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                try {
                    serializer.write(p, file);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*Intent intent = new Intent(ActivitySendXML.this, MyService.class);
                // Mettre en place un receiver
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(MyService.MON_ACTION);
                registerReceiver(new MonReceiver(), intentFilter);
                // Envoyer la valeur initiale du compteur
                Bundle b=new Bundle();
                b.putInt("compteur", 0);
                intent.putExtras(b);
                startService(intent);*/
            }
        });

        edtPrenom = (EditText)findViewById(R.id.Prenom);
        edtNom = (EditText)findViewById(R.id.Nom);
        edtAge = (EditText)findViewById(R.id.Age);

    }

    // classe interne pour les echanges avec le service
    private class MonReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            int compteur = arg1.getIntExtra("compteur", 0);
            Toast.makeText(ActivitySendXML.this,
                    "Depuis le service  !\n"
                            + " compteur: " + String.valueOf(compteur),
                    Toast.LENGTH_LONG).show();
        }
    }
}
