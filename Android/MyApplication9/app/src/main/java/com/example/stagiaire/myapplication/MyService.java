package com.example.stagiaire.myapplication;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MyService extends Service {
    int compteur = 0;
    final static String MON_ACTION = "COMPTAGE";
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    Intent intent2;

    @Override
    public int onStartCommand(Intent intent, int flags, int startid){
        Bundle b=intent.getExtras();
        compteur = b.getInt("compteur");
        Log.i("Service compteur = ", String.valueOf(compteur));
        Toast.makeText(this, "Compteur initial", Toast.LENGTH_SHORT).show();
        // retourner la reponse
        intent2 = new Intent();
        intent2.setAction(MON_ACTION);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        intent2.putExtra("compteur", ++compteur);
                        sendBroadcast(intent2);
                        Thread.sleep(100);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return super.onStartCommand(intent, flags, startid);
    }
}