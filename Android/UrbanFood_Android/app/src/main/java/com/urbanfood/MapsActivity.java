

package com.urbanfood;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.urbanfood.urbanfood.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


import adapters.MenuItemRecyclerViewAdapter;
import adapters.UrbanFoodApplication;
import classes.MenuItem;
import classes.Spot;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String CODE_HEXA_FOND_COMPTE = "#c3c2c5";
    private static final String CODE_HEXA_FOND_BOULANGERIE = "#fdae01";
    private static final String CODE_HEXA_FOND_POTAGER = "#4c9e0d";
    private static final String CODE_HEXA_FOND_SOURCE = "#1976D2";
    private static final String CODE_HEXA_FOND_OPTIONS =  "#636f73";
    private static final String CODE_HEXA_FOND_TOUS = "#832133";
    private static final String CODE_HEXA_FOND_APROPOS = "#603f6a";
    private static final String CODE_HEXA_FOND_DECONNEXION = "#8dbad8";
    private static final String CODE_HEXA_FOND_CUEILLETTE = "#ff9a7a";



    private int _currentIndex = 0;
    private GoogleMap mMap;

    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private RecyclerView recyclerView;
    private SupportMapFragment mapFragment;
    private RelativeLayout layoutMapControls;
    private String _typeconsultation = "";
    private List<Spot> listeSpots;

    private TextView txtNomSpot;
    private TextView txtDescriptionSpot;
    private TextView txtLatitude;
    private TextView txtLongitude;
    private ImageView imgTypeSpot;
    private TextView txtIndex;
    private boolean voirtous = false;
    private LinearLayout linearLayoutHeader;
    private URL url;
    private StringBuffer response;
    private String responseText;
    String[] typesArray = {"Potager","Cueillette","Source","Invendu"};


    final String SERVICES_PATH = "http://88.169.107.232:8888/SowizServicesHost/SowizServices.svc/ListeSpots?";
    final String SERVICES_PATH_RECORDSPOT = "http://88.169.107.232:8888/SowizServicesHost/SowizServices.svc/EnregistrerLieu?";

    private ImageButton imgbtnCommentaires;
    private TextView txtUtilisateur;
    private TextView txtNombreCommentaires;

    private LocationManager manager;
    private LocationListener locationListener;
    private Location mylocation;
    private Spinner spTypesSpot;
    private LinearLayout linearLayoutHeaderAjoutSpot;
    private ImageView imgTypeSpotAjoutSpot;
    private TextView txtLongitudeSpotAjout;
    private TextView txtLatitudeSpotAjout;
    private ImageButton imagebtnAnnulerEnregistrementLieu;
    private ImageButton imagebtnValiderEnregistrementLieu;
    private EditText edtNomNouveauSpot;
    private EditText edtDescriptionNouveauSpot;
    //private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );


        listeSpots = new ArrayList<Spot>();

        //mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        txtNomSpot = (TextView) findViewById(R.id.textViewNomSpot);
        txtDescriptionSpot= (TextView) findViewById(R.id.descriptionSpot);
        txtLatitude= (TextView) findViewById(R.id.latitudeSpot);
        txtLongitude= (TextView) findViewById(R.id.longitudeSpot);
        txtNombreCommentaires = (TextView) findViewById(R.id.textViewNombreCommentaires);
        imgTypeSpot = (ImageView) findViewById(R.id.imageViewTypeSpot);
        imgTypeSpotAjoutSpot = (ImageView) findViewById(R.id.imageViewTypeSpotAjout);
        txtIndex = (TextView)findViewById(R.id.textViewIndex);
        txtUtilisateur = (TextView)findViewById(R.id.textViewUtilisateur);
        imgbtnCommentaires = (ImageButton)findViewById(R.id.imageButtonCommentaires);
        imgbtnCommentaires.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(getApplicationContext(), CommentairesActivity.class);
                Bundle b = new Bundle();
                it.putExtra("idspot", listeSpots.get(_currentIndex).getIdLieu());
                startActivity(it);
            }
        });

        linearLayoutHeader = (LinearLayout)findViewById(R.id.Header);
        linearLayoutHeaderAjoutSpot = (LinearLayout)findViewById(R.id.HeaderAjoutSpot);
        txtLatitudeSpotAjout = (TextView) findViewById(R.id.latitudeSpotAjout);
        txtLongitudeSpotAjout = (TextView) findViewById(R.id.longitudeSpotAjout);


        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);

        List<MenuItem> gaggeredList = null;
        // a remplacer par mode invité : utiliser enum
        Boolean b = false;
        if(b)
            gaggeredList = getListMenuItemsGuest();
        else
            gaggeredList = getListMenuItems();


        MenuItemRecyclerViewAdapter rcAdapter = new MenuItemRecyclerViewAdapter(MapsActivity.this, gaggeredList);
        recyclerView.setAdapter(rcAdapter);



        layoutMapControls = (RelativeLayout)findViewById(R.id.mapcontrols);
        layoutMapControls.setVisibility(View.GONE);

        ImageButton btnImageFirstSpot = (ImageButton)findViewById(R.id.imageButtonFirstSpot);
        btnImageFirstSpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _currentIndex = 0;
                setCurrentSpot(listeSpots.get(0));

            }
        });

        ImageButton btnImagePreviousSpot = (ImageButton)findViewById(R.id.imageButtonPreviousSpot);
        btnImagePreviousSpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _currentIndex--;
                if(_currentIndex < 0)
                    _currentIndex = listeSpots.size() - 1;
                setCurrentSpot(listeSpots.get(_currentIndex));

            }
        });

        ImageButton btnImageNextSpot = (ImageButton)findViewById(R.id.imageButtonNextSpot);
        btnImageNextSpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _currentIndex++;
                if(_currentIndex > listeSpots.size() - 1)
                    _currentIndex = 0;
                setCurrentSpot(listeSpots.get(_currentIndex));

            }
        });

        ImageButton btnImageLastSpot = (ImageButton)findViewById(R.id.imageButtonLastSpot);
        btnImageLastSpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _currentIndex= listeSpots.size() - 1;
                setCurrentSpot(listeSpots.get(_currentIndex));

            }
        });

        spTypesSpot = (Spinner) findViewById(R.id.spTypeSpots);
        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, typesArray);
        spTypesSpot.setAdapter(adapter );
        spTypesSpot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(((CheckedTextView)view).getText().equals("Potager")) {
                    imgTypeSpotAjoutSpot.setImageResource(R.mipmap.carrot_rounded);
                    linearLayoutHeaderAjoutSpot.setBackgroundResource(R.drawable.layout_bg_potager);
                }
                else if(((CheckedTextView)view).getText().equals("Source"))
                {
                    imgTypeSpotAjoutSpot.setImageResource(R.mipmap.drop_rounded);
                    linearLayoutHeaderAjoutSpot.setBackgroundResource(R.drawable.layout_bg_source);
                }
                else if(((CheckedTextView)view).getText().equals("Invendu"))
                {
                    imgTypeSpotAjoutSpot.setImageResource(R.mipmap.bread_rounded);
                    linearLayoutHeaderAjoutSpot.setBackgroundResource(R.drawable.layout_bg_invendus);
                }
                else if(((CheckedTextView)view).getText().equals("Cueillette"))
                {
                    imgTypeSpotAjoutSpot.setImageResource(R.mipmap.cherry_rounded);
                    linearLayoutHeaderAjoutSpot.setBackgroundResource(R.drawable.layout_bg_cueillette);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        edtNomNouveauSpot = (EditText)findViewById(R.id.editTextNouveauSpotNom);
        edtDescriptionNouveauSpot = (EditText)findViewById(R.id.editTextNouveauSpotDescription);

        imagebtnValiderEnregistrementLieu = (ImageButton)findViewById(R.id.imageButtonValiderEnregistrementSpot);
        imagebtnValiderEnregistrementLieu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean cancel = false;
                if(edtDescriptionNouveauSpot.getText().length() ==0) {
                    txtDescriptionSpot.setError("Vous devez renseigner une description");
                    cancel = true;
                }
                if(edtNomNouveauSpot.getText().length() == 0)
                {
                    edtNomNouveauSpot.setError("Vous devez renseigner un nom");
                    cancel = true;
                }
                if(!cancel)
                {
                    String[] strPolicesArray = getResources().getStringArray(R.array.TypesGlanage);

                    new RecordSpotTask(UrbanFoodApplication.getCurrentUser().getMail(), edtNomNouveauSpot.getText().toString(),
                            edtDescriptionNouveauSpot.getText().toString(), mylocation.getLatitude(),
                            mylocation.getLongitude(),strPolicesArray[spTypesSpot.getSelectedItemPosition()]).execute();
                }
            }
        });

        imagebtnAnnulerEnregistrementLieu = (ImageButton)findViewById(R.id.imageButtonAnnulerEnregistrementSpot);
        imagebtnAnnulerEnregistrementLieu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showCancelPopUp();
            }
        });
        initMenuFlottant();
    }

    private void showCancelPopUp() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Annuler la procédure d'enregistrement ?")
                .setCancelable(false)
                .setTitle("Annulation")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        //Stop the activity
                        edtNomNouveauSpot.setText("");
                        edtDescriptionNouveauSpot.setText("");
                        LinearLayout layoutMapSpot = (LinearLayout) findViewById(R.id.detailsmarker);
                        layoutMapSpot.setVisibility(View.VISIBLE);
                        LinearLayout layoutAjoutSpot = (LinearLayout) findViewById(R.id.ajoutSpot);
                        layoutAjoutSpot.setVisibility(View.GONE);
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    class RecordSpotTask extends AsyncTask<Object, Void, Spot>
    {

        private ProgressDialog progressDialog;
        String _mail, _nom, _description, _type;
        double _latitude, _longitude;

        public RecordSpotTask(String mail, String nom, String description, double latitude, double longitude, String type) {
            this._mail = mail;
            this._nom = nom;
            this._description = description;
            this._latitude = latitude;
            this._longitude = longitude;
            this._type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressDialog = new ProgressDialog(MapsActivity.this);
            progressDialog.setMessage("Enregistrement en cours...");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected Spot doInBackground(Object[] objects) {
            return getWebServiceRecordSpotResponse(_mail, _nom, _description, _latitude, _longitude, _type);
        }

        @Override
        protected void onPostExecute(Spot result) {
            super.onPostExecute(result);
            if(progressDialog.isShowing())
                progressDialog.dismiss();

            edtDescriptionNouveauSpot.setError(null);
            edtNomNouveauSpot.setError(null);

            LinearLayout layoutMapSpot = (LinearLayout) findViewById(R.id.detailsmarker);
            layoutMapSpot.setVisibility(View.VISIBLE);
            LinearLayout layoutAjoutSpot = (LinearLayout) findViewById(R.id.ajoutSpot);
            layoutAjoutSpot.setVisibility(View.GONE);
            manager.removeUpdates(locationListener);
            // Dismiss the progress dialog





        }
    }

    protected Spot getWebServiceRecordSpotResponse(String mail, String nom, String description, double latitude, double longitude, String type) {

        try {

            url = new URL(SERVICES_PATH_RECORDSPOT + "mail="+ mail+"&nom=" +nom.replaceAll(" ", "%20").replaceAll("'", "%27") +"&description=" + description.replaceAll(" ", "%20").replaceAll("'", "%27") +"&latitude=" + latitude + "&longitude=" + longitude +"&type=" + type  );

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();

            Log.d("TAG", "Response code: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                // Reading response from input Stream
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String output;
                response = new StringBuffer();

                while ((output = in.readLine()) != null) {
                    response.append(output);
                }
                in.close();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }



        responseText = response.toString();
        Spot s = new Spot();
        responseText = response.toString();
        //Call ServerData() method to call webservice and store result in response
        //  response = service.ServerData(path, postDataParams);
        Log.d("TAG", "data:" + responseText);
        try {
            JSONObject jsonobject = new JSONObject(responseText);
            s.setDateCreation(jsonobject.getString("DateCreation"));
            s.setLatitude(jsonobject.getDouble("Latitude"));
            s.setLongitude(jsonobject.getDouble("Longitude"));
            s.setNomLieu(jsonobject.getString("LieuNom"));
            s.setUtilisateur(jsonobject.getString("Utilisateur"));
            s.setDescriptionLieu(jsonobject.getString("Description"));
            s.setIdLieu(jsonobject.getInt("LieuCode"));
            s.setTypeLieu(jsonobject.getString("Type"));
            s.setNombreCommentaires(jsonobject.getInt("NombreCommentaires"));
            s.setVille(jsonobject.getString("Ville"));






        } catch (JSONException e) {
            e.printStackTrace();
        }


        return s;
    }

    private void setCurrentSpot(Spot spot) {

        mMap.clear();
        voirtous = false;
        mMap.addMarker(new MarkerOptions().position(new LatLng(spot.getLatitude(), spot.getLongitude())).icon(BitmapDescriptorFactory.fromBitmap(getMarkerIcon(spot.getTypeLieu()))).title(spot.getNomLieu()));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(spot.getLatitude(), spot.getLongitude()), 12.0f));

        txtNomSpot.setText(spot.getNomLieu());
        txtDescriptionSpot.setText(spot.getDescriptionLieu());
        if(spot.getTypeLieu().equals("Potagers")) {
            imgTypeSpot.setImageResource(R.mipmap.carrot_rounded);
            linearLayoutHeader.setBackgroundResource(R.drawable.layout_bg_potager);
        }
        else if(spot.getTypeLieu().equals("Sources"))
        {
            imgTypeSpot.setImageResource(R.mipmap.drop_rounded);
            linearLayoutHeader.setBackgroundResource(R.drawable.layout_bg_source);
        }
        else if(spot.getTypeLieu().equals("Invendus"))
        {
            imgTypeSpot.setImageResource(R.mipmap.bread_rounded);
            linearLayoutHeader.setBackgroundResource(R.drawable.layout_bg_invendus);
        }
        else if(spot.getTypeLieu().equals("Cueillette"))
        {
            imgTypeSpot.setImageResource(R.mipmap.cherry_rounded);
            linearLayoutHeader.setBackgroundResource(R.drawable.layout_bg_cueillette);
        }

        txtLatitude.setText("Latitude : " + String.valueOf(spot.getLatitude()));
        txtLongitude.setText("Longitude : " + String.valueOf(spot.getLongitude()));
        txtIndex.setText(String.valueOf(_currentIndex + 1) + "/" + listeSpots.size());
        txtNombreCommentaires.setText(String.valueOf(spot.getNombreCommentaires()));
        txtUtilisateur.setText("Identifié par : " + spot.getUtilisateur() + ", le " + spot.getDateFormatted());

    }

    private void initMenuFlottant() {

        FloatingActionButton actionVoirTous = (FloatingActionButton) findViewById(R.id.action_VoirTous);
        actionVoirTous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(voirtous)
                {
                    setCurrentSpot(listeSpots.get(_currentIndex));
                    voirtous =false;
                }
                else
                {
                    AfficherListe();
                    voirtous = true;
                }

            }
        });

        AddFloatingActionButton actionAjouterLieu = (AddFloatingActionButton) findViewById(R.id.action_AjouterLieu);
        actionAjouterLieu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !manager.isProviderEnabled( LocationManager.NETWORK_PROVIDER ) ) {
                    buildAlertMessageNoGps();
                }
                else
                {
                    manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,500,0,locationListener);
                    mMap.setMyLocationEnabled(true);
                }

            }
        });

        ShapeDrawable drawable = new ShapeDrawable(new OvalShape());
        drawable.getPaint().setColor(getResources().getColor(R.color.white));

        final FloatingActionButton actionZoomIn = (FloatingActionButton) findViewById(R.id.action_ZoomIn);
        actionZoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
            }
        });

        final FloatingActionButton actionZoomOut = (FloatingActionButton) findViewById(R.id.action_ZoomOut);
        actionZoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.animateCamera(CameraUpdateFactory.zoomOut());
            }
        });

    }



    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("L'ajout d'un spot Urban Food nécessite que votre GPS soit actif. Souhaitez-vous l'activer ?")
                .setCancelable(false)
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Bundle b = new Bundle();
                        b.putBoolean("gps", true);
                        Intent newIntent = new Intent();
                        newIntent.setAction(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        newIntent.putExtras(b);
                        startActivityForResult(newIntent, 1000);
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        //super.onActivityResult(requestCode, resultCode, data);
        if( manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,500,0,locationListener);
            mMap.setMyLocationEnabled(true);
            //CameraUpdateFactory.newLatLng(new LatLng(l.getLatitude(), l.getLongitude()));

        }

    }

    private void initLocationListener() {
        // TODO Auto-generated method stub
        locationListener = new LocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onProviderEnabled(String provider) {
                // TODO Auto-generated method stub

                //manager.requestLocationUpdates(provider,0,0,locationListener);


            }

            @Override
            public void onProviderDisabled(String provider) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onLocationChanged(Location location) {
                // TODO Auto-generated method stub
                //Toast.makeText(getActivity(), "location changed : latitude => "+location.getLatitude()+", longitude => "+location.getLongitude(), 1000).show();
                Log.d("Message", "location changed : latitude => "+location.getLatitude()+", longitude => "+location.getLongitude());
                LinearLayout layoutMapSpot = (LinearLayout) findViewById(R.id.detailsmarker);
                layoutMapSpot.setVisibility(View.GONE);
                LinearLayout layoutAjoutSpot = (LinearLayout) findViewById(R.id.ajoutSpot);
                layoutAjoutSpot.setVisibility(View.VISIBLE);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()),
                        15));
                mylocation = location;
                manager.removeUpdates(locationListener);

                txtLatitudeSpotAjout.setText("Lat. : "+String.valueOf(location.getLatitude()));
                txtLongitudeSpotAjout.setText("Long. : " + String.valueOf(location.getLongitude()));
                mylocation = location;

            }
        };


    }

    private void AfficherListe() {
        mMap.clear();
        for (int i = 0; i <= listeSpots.size() - 1; i++)
        {
            mMap.addMarker(new MarkerOptions().position(new LatLng(listeSpots.get(i).getLatitude(), listeSpots.get(i).getLongitude())).icon(BitmapDescriptorFactory.fromBitmap(getMarkerIcon(listeSpots.get(i).getTypeLieu()))));
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(listeSpots.get(0).getLatitude(), listeSpots.get(0).getLongitude()), 15));
    }


    private List<MenuItem> getListMenuItemsGuest() {
        List<MenuItem> listViewItems = new ArrayList<MenuItem>();
        listViewItems.add(new MenuItem("Potagers", R.drawable.carrot_64, CODE_HEXA_FOND_POTAGER));
        listViewItems.add(new MenuItem("Cueillette", R.drawable.cherry_64, CODE_HEXA_FOND_CUEILLETTE));
        listViewItems.add(new MenuItem("Sources", R.drawable.drop_64, CODE_HEXA_FOND_SOURCE));
        listViewItems.add(new MenuItem("Invendus", R.drawable.bread_64, CODE_HEXA_FOND_BOULANGERIE));
        listViewItems.add(new MenuItem("Tous", R.drawable.tous_64, CODE_HEXA_FOND_TOUS));
        listViewItems.add(new MenuItem("A propos", R.drawable.settings_64, CODE_HEXA_FOND_OPTIONS));
        listViewItems.add(new MenuItem("Quitter", R.drawable.logout_64, CODE_HEXA_FOND_DECONNEXION));
        return listViewItems;
    }

    private List<MenuItem> getListMenuItems() {
        List<MenuItem> listViewItems = new ArrayList<MenuItem>();
        listViewItems.add(new MenuItem("Potagers", R.drawable.carrot_64, CODE_HEXA_FOND_POTAGER));
        listViewItems.add(new MenuItem("Cueillette", R.drawable.cherry_64, CODE_HEXA_FOND_CUEILLETTE));
        listViewItems.add(new MenuItem("Sources", R.drawable.drop_64, CODE_HEXA_FOND_SOURCE));
        listViewItems.add(new MenuItem("Invendus", R.drawable.bread_64, CODE_HEXA_FOND_BOULANGERIE));
        listViewItems.add(new MenuItem("Tous", R.drawable.tous_64, CODE_HEXA_FOND_TOUS));
        listViewItems.add(new MenuItem("Mon Compte", R.drawable.user_64, CODE_HEXA_FOND_COMPTE));
        listViewItems.add(new MenuItem("A propos", R.drawable.information_64, CODE_HEXA_FOND_APROPOS));
        listViewItems.add(new MenuItem("Deconnexion", R.drawable.logout_64, CODE_HEXA_FOND_DECONNEXION));
        return listViewItems;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        initLocationListener();


    }

    private Bitmap getMarkerIcon(String type)
    {
        Bitmap bmp = null;
        if(type.equals("Potagers"))
            bmp = BitmapFactory.decodeResource(getResources(),R.mipmap.location_potager);
        else if(type.equals("Sources"))
            bmp = BitmapFactory.decodeResource(getResources(),R.mipmap.location_source);
        else if(type.equals("Invendus"))
            bmp = BitmapFactory.decodeResource(getResources(),R.mipmap.location_bread);
        else if(type.equals("Cueillette"))
            bmp = BitmapFactory.decodeResource(getResources(),R.mipmap.location_cueillette);

        return bmp;
    }


    private void getListeSpots(String type){
        listeSpots = new ArrayList<Spot>();
        new GetListeSpotsData(type).execute();
    }

    public void AfficherCarte(String type) {
        _typeconsultation = type;
        _currentIndex = 0;
        recyclerView.setVisibility(View.GONE);

        listeSpots.clear();
        getListeSpots(type);

    }

    @Override
    public void onBackPressed() {

        if(layoutMapControls.getVisibility()==View.VISIBLE)
        {
            layoutMapControls.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        else
        {
            ShowInviteFermeture();

        }

    }

    public void ShowInviteFermeture() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.quit)
                .setMessage(R.string.quit_confirm)
                .setPositiveButton(R.string.confirmer, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Stop the activity
                        MapsActivity.this.finish();
                        System.exit(0);
                    }

                })
                .setNegativeButton(R.string.annuler, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Stop the activity
                        Toast.makeText(getApplicationContext(), "Annulation de la fermeture", Toast.LENGTH_LONG).show();
                    }

                })
                .show();

    }

    public void OuvrirAPropos() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void AfficherOptions() {

    }

    public void AfficherMonCompte() {
        Intent intent = new Intent(this, MonCompteActivity.class);
        startActivity(intent);
    }

    class GetListeSpotsData extends AsyncTask
    {

        private ProgressDialog progressDialog;
        String _type;

        public GetListeSpotsData(String type) {
            this._type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressDialog = new ProgressDialog(MapsActivity.this);
            progressDialog.setMessage("Chargement des spots Urban Food...");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected Object doInBackground(Object[] objects) {
            return getWebServiceResponseData(_type);
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            // Dismiss the progress dialog
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            listeSpots = (List<Spot>) o;
            layoutMapControls.setVisibility(View.VISIBLE);
            if(listeSpots.size()> 0 )
                setCurrentSpot(listeSpots.get(_currentIndex));

        }
    }
    protected List<Spot> getWebServiceResponseData(String type) {

        try {

            url = new URL(SERVICES_PATH +"type="+ type);
            Log.d("TAG", "ServerData: " + SERVICES_PATH +"?type="+ type);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();

            Log.d("TAG", "Response code: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                // Reading response from input Stream
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String output;
                response = new StringBuffer();

                while ((output = in.readLine()) != null) {
                    response.append(output);
                }
                in.close();
            }}
        catch(Exception e){
            e.printStackTrace();
        }
        ArrayList<Spot> spots = new ArrayList<Spot>();
        responseText = response.toString();
        //Call ServerData() method to call webservice and store result in response
        //  response = service.ServerData(path, postDataParams);
        Log.d("TAG", "data:" + responseText);
        try {
            JSONArray jsonarray = new JSONArray(responseText);
            for (int i = 0; i < jsonarray.length(); i++) {
                JSONObject jsonobject = jsonarray.getJSONObject(i);

                Spot s = new Spot();
                s.setDateCreation(jsonobject.getString("DateCreation"));
                s.setLatitude(jsonobject.getDouble("Latitude"));
                s.setLongitude(jsonobject.getDouble("Longitude"));
                s.setNomLieu(jsonobject.getString("LieuNom"));
                s.setUtilisateur(jsonobject.getString("Utilisateur"));
                s.setDescriptionLieu(jsonobject.getString("Description"));
                s.setIdLieu(jsonobject.getInt("LieuCode"));
                s.setTypeLieu(jsonobject.getString("Type"));
                s.setNombreCommentaires(jsonobject.getInt("NombreCommentaires"));
                s.setVille(jsonobject.getString("Ville"));



                spots.add(s);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return spots;
    }

}
