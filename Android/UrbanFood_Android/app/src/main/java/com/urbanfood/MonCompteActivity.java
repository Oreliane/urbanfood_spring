package com.urbanfood;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;


import com.urbanfood.urbanfood.R;

import anims.ZoomOutPageTransformer;
import classes.Polices;
import fragments.MonComptePagerFragment;
import views.ViewPagerCustomDuration;

public class MonCompteActivity extends FragmentActivity {


    private ViewPagerCustomDuration mPager;
    /**
     * Le pager adapter, qui fournit les pages au viewpager.
     */
    private PagerAdapter mPagerAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mon_compte);
        Bundle extras = getIntent().getExtras();
        Polices.setContext(getApplicationContext());
        initViews();


    }

    private void initViews() {
        // TODO Auto-generated method stub
        mPager = (ViewPagerCustomDuration) findViewById(R.id.viewpagerMonCompte);
        mPager.setScrollDurationFactor(4);
        mPagerAdapter = new MonComptePagerAdapter(getSupportFragmentManager());
        getPager().setAdapter(mPagerAdapter);
        getPager().setPageTransformer(true, new ZoomOutPageTransformer());
        getPager().setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.clear();
        return true;
    }

    /**
     * Le view pager qui recoit les pages et gère la transition entre elles.
     */
    public ViewPager getPager() {
        return mPager;
    }

    private class MonComptePagerAdapter extends FragmentStatePagerAdapter {
        public MonComptePagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            MonComptePagerFragment fragment = new MonComptePagerFragment();
            Bundle bundle = new Bundle(1);
            bundle.putInt("position", position);

            fragment.setArguments(bundle);
            return fragment;

        }

        @Override
        public int getCount() {
            return 2;
        }



    }

    @Override
    public void onBackPressed() {
        if(mPager.getCurrentItem()==0)
            finish();
        else
            mPager.setCurrentItem(0, true);
    }
}
