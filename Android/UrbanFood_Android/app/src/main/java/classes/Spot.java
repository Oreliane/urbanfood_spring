package classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrateur on 11/09/2017.
 */

public class Spot {

    private int _idLieu=0;
    private int _iduser=0;
    private int _idVille=0;
    private String _typeLieu="";
    private String _nomLieu="";
    private String _descriptionLieu="";
    private String _dateCrea;
    private double _latitude=0;
    private double _longitude=0;
    private String _ville;
    private int _nbrecommentaires;
    private String _utilisateur;
    private String _dateformatted;

    public Spot(){
    }

    public Spot(int idLieu, int iduser, String typeLieu, int idVille, String nomLieu, String descriptionLieu,
                String dateCrea, double latitude, double longitude){

        this._idLieu= idLieu;
        this._iduser= iduser;
        this._typeLieu=typeLieu;
        this._idVille=idVille;
        this._nomLieu=nomLieu;
        this._descriptionLieu=descriptionLieu;
        this._dateCrea=dateCrea;
        this._latitude=latitude;
        this._longitude=longitude;

    }

    public int getIdLieu() {
        return _idLieu;
    }

    public void setIdLieu(int idLieu) {
        this._idLieu = idLieu;
    }

    public int getIduser() {
        return _iduser;
    }

    public void setIduser(int _iduser) {
        this._iduser = _iduser;
    }

    public String getTypeLieu() {
        return _typeLieu;
    }

    public void setTypeLieu(String typeLieu) {
        this._typeLieu = typeLieu;
    }

    public int getIdVille() {
        return _idVille;
    }

    public void setIdVille(int idVille) {
        this._idVille = idVille;
    }

    public String getNomLieu() {
        return _nomLieu;
    }

    public void setNomLieu(String nomLieu) {
        this._nomLieu = nomLieu;
    }

    public String getDescriptionLieu() {
        return _descriptionLieu;
    }

    public void setDescriptionLieu(String descriptionLieu) {
        this._descriptionLieu = descriptionLieu;
    }

    public String getDateCreation() {
        return _dateCrea;
    }

    public void setDateCreation(String dateCrea) {
        this._dateCrea = dateCrea;
        formateDate(dateCrea);
    }

    public double getLatitude() {
        return _latitude;
    }

    public void setLatitude(double latitude) {
        this._latitude = latitude;
    }

    public double getLongitude() {
        return _longitude;
    }

    public void setLongitude(double longitude) {
        this._longitude = longitude;
    }

    public void setVille(String ville) {
        this._ville = ville;
    }

    public String getVille() {
        return this._ville;
    }

    public void setNombreCommentaires(int nombreCommentaires) {
        this._nbrecommentaires = nombreCommentaires;
    }

    public int getNombreCommentaires() {
        return this._nbrecommentaires;
    }

    public void setUtilisateur(String util) {
        this._utilisateur = util;
    }

    public String getUtilisateur() {
        return this._utilisateur;
    }

    private void formateDate(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = formatter.parse(date);
            formatter = new SimpleDateFormat("dd/MM/yyyy à HH:mm:ss");

            setDateFormatted(formatter.format(d));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setDateFormatted(String dateFormatted) {
        this._dateformatted = dateFormatted;
    }

    public String getDateFormatted() {
        return _dateformatted;
    }
}
