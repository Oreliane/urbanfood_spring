package classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrateur on 13/09/2017.
 */

public class Utilisateur {

    private String _login ="";
    private String _mail ="";
    private String _password ="";
    private Date _datecreation = null;
    private int _id;
    private String _connexionrequestresult;
    private String _dateformatted;

    public Utilisateur()
    {


    }

    public Utilisateur(int id, String login, String mail, String password, Date datecreation)
    {
        this._id= id;
        this._login = login;
        this._mail = mail;
        this._password = password;
        this._datecreation = datecreation;
        formateDate(_datecreation);
    }

    public String getLogin() {
        return _login;
    }

    public void setLogin(String login) {
        this._login = login;
    }

    public String getMail() {
        return _mail;
    }

    public void setMail(String mail) {
        this._mail = mail;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String password) {
        this._password = password;
    }

    public Date getDateCreation() {
        return _datecreation;
    }

    public void setDateCreation(Date datecreation) {
        this._datecreation = datecreation;
        formateDate(_datecreation);
    }

    private void formateDate(Date datecreation) {


        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy à HH:mm:ss");
        setDateFormatted(formatter.format(datecreation));
    }


    public int getId() {
        return _id;
    }

    public void setId(int id) {
        this._id = id;
    }

    public String getConnexionRequestResult() {
        return _connexionrequestresult;
    }

    public void setConnexionRequestResult(String connexionrequestresult) {
        this._connexionrequestresult = connexionrequestresult;
    }

    public void setDateFormatted(String date) {
        this._dateformatted = date;
    }

    public String getDateFormatted() {
        return this._dateformatted;
    }
}
