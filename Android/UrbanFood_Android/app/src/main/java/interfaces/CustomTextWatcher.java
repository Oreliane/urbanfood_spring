package interfaces;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Spoon on 22/09/2017.
 */

 public class CustomTextWatcher implements TextWatcher {

    boolean mIsEnabled = true;

    public void setEnabled(boolean enabled) {
        mIsEnabled = enabled;
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (!mIsEnabled) return;

    }
}