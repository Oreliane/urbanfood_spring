package interfaces;

import java.util.List;

import classes.Commentaire;
import classes.Spot;

/**
 * Created by Administrateur on 11/09/2017.
 */

public interface IRecordable {

    /* 1er sprint */
    public Boolean Authentification(String mail, String password);
    public List<Spot> getListeSpots();
    public List<Spot> getListeSpots(String type);

    /* 2ieme sprint */
    public Boolean InscriptionUtilisateur(String login, String mail, String password);
    public Boolean EditerMotDePasse(String mail, String password);
    public Boolean AjouterSpot(String mail, Double latitude, Double longitude, String nom, String description);

    /* 3ieme sprint */
    public List<Commentaire> getListeCommentaires(int idspot);
    public Boolean AjouterCommentaire(int idspot);
    public Boolean SupprimerCommentaire(int idcommentaire);
    public Boolean EditerCommentaire(int idcommentaire, String commentaire);


}
