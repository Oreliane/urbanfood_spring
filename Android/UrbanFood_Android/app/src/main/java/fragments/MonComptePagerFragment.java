package fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.urbanfood.LoginActivity;
import com.urbanfood.MonCompteActivity;
import com.urbanfood.urbanfood.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import AsyncTasks.GetListeCommentairesData;
import adapters.UrbanFoodApplication;
import classes.Commentaire;
import classes.ImageHelper;
import classes.Polices;
import classes.Spot;
import interfaces.CustomTextWatcher;
import views.CircularImageView;


public class MonComptePagerFragment extends Fragment implements OnMapReadyCallback{

    private static final String FONT = "Alex Brush";
    private static GoogleMap mMap;
    private ViewGroup rootView;

    private TextView txtNbreLieuxCrees;
    private TextView txtNbreLieuxCommentes;
    private TextView txtLibActivite;
    private EditText edtLogin;
    private Button btnUpdateUser;
    private EditText edtPassword;
    private EditText edtMail;
    private TextView txtDateCreationCompte;
    private URL url;
    private Button btnSupprimerCompte;
    private TextView txtTotalCommentairesUtilisateur;
    final String SERVICES_PATH = "http://88.169.107.232:8888/SowizServicesHost/SowizServices.svc/";
    private StringBuffer response;
    private String responseText;


    private Button btnVisuNbreLieuxCrees;
    private Button btnVisuNbreTotalCommentaires;
    private Button btnNbreLieuxCommentes;

    private static ImageView imgTypeSpot;
    private static TextView txtIndexMap;
    private static TextView txtNomSpot;
    private static TextView txtDescriptionSpot;
    private static TextView txtLatitudeSpot;
    private static TextView txtLongitudeSpot;
    private static TextView textViewNombreCommentaires;

    private static ArrayList<Spot> listeSpotsCrees;
    private static ArrayList<Spot> listeSpotsCommentes;
    private static HashMap<Spot, ArrayList<Commentaire>> listeCommentairesUtilisateurs;

    private static TextView txtViewUtilisateur;
    private static RelativeLayout layoutNombreCommentaires;
    private static ImageButton imageButtonPreviousSpot;
    private static ImageButton imageButtonNextSpot;
    private static ImageButton imageButtonFirstSpot;
    private static ImageButton imageButtonLastSpot;
    private static int indexListeLieuxCrees = 0;
    private static LinearLayout linearLayoutHeader;
    private static String _typelieu  ="";
    private static SupportMapFragment mapFragment;
    private static int indexListeLieuxCommentes = 0;
    private static RelativeLayout layoutControlesCommentaires;
    private static EditText edtTextCommentaire;
    private int indexcommentaires = 0;
    private int indexTotalCommentaires = 0;
    private static LinearLayout layoutcontrolesDefilementCommentaires;
    private static ImageButton imageButtonCommentaireSuivant;
    private static LinearLayout layoutControlesEnregistrementCommentaire;
    private static ImageButton imageButtonCommentairePrecedent;
    private static ArrayList<Commentaire> listeAllComments;
    private Spot currentSpot;
    private static ImageButton imageButtonCancelModifDeleteComment;
    private static ImageButton imageButtonUpdateComment;

    public MonComptePagerFragment() {
        // TODO Auto-generated constructor stub

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        int position = getArguments().getInt("position");

        listeAllComments = new ArrayList<Commentaire>();
        listeCommentairesUtilisateurs = new HashMap<Spot, ArrayList<Commentaire>>();

        rootView = initViews(position, inflater, container);
        return rootView;
    }

    CustomTextWatcher textWatcher=new CustomTextWatcher();

    private ViewGroup initViews(int position, LayoutInflater inflater, ViewGroup container) {
        // TODO Auto-generated method stub
        ViewGroup v = null;
        if(position ==0) {
            v = (ViewGroup) inflater.inflate(
                    R.layout.fragment_mon_compte, container, false);
            v = initViewsMonCompte(v);
        }else if (position ==1) {
            v = (ViewGroup) inflater.inflate(
                    R.layout.fragment_map, container, false);
            v = initViewsVisualisationCarte(v);
        }

        return v;
    }

    private ViewGroup initViewsVisualisationCarte(ViewGroup v) {
        imgTypeSpot = (ImageView)v.findViewById(R.id.imageViewTypeSpot);
        txtIndexMap = (TextView) v.findViewById(R.id.textViewIndex);
        txtNomSpot = (TextView) v.findViewById(R.id.textViewNomSpot);
        txtDescriptionSpot = (TextView) v.findViewById(R.id.descriptionSpot);
        txtLatitudeSpot =  (TextView) v.findViewById(R.id.latitudeSpot);
        txtLongitudeSpot = (TextView) v.findViewById(R.id.longitudeSpot);
        txtViewUtilisateur = (TextView) v.findViewById(R.id.textViewUtilisateur);
        layoutNombreCommentaires = (RelativeLayout)v.findViewById(R.id.layoutNombreCommentaires);
        linearLayoutHeader = (LinearLayout) v.findViewById(R.id.Header);
        layoutControlesCommentaires = (RelativeLayout) v.findViewById(R.id.controlesCommentaire) ;
        textViewNombreCommentaires = (TextView) v.findViewById(R.id.textViewNombreCommentaires);
        edtTextCommentaire = (EditText) v.findViewById(R.id.edtTextCommentaire);
        edtTextCommentaire.addTextChangedListener(textWatcher);
        mapFragment =(SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        layoutControlesEnregistrementCommentaire = (LinearLayout) v.findViewById(R.id.controlesEnregistrementCommentaire);

        layoutcontrolesDefilementCommentaires = (LinearLayout) v.findViewById(R.id.controlesDefilementCommentaires);
        imageButtonCommentairePrecedent = (ImageButton)v.findViewById(R.id.imageButtonUpComment);
        imageButtonCommentairePrecedent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexcommentaires--;
                if(indexcommentaires < 0)
                    indexcommentaires = listeCommentairesUtilisateurs.get(listeCommentairesUtilisateurs.keySet().toArray()[indexListeLieuxCommentes]).size()-1;
                setFields();
            }
        });

        imageButtonCommentaireSuivant = (ImageButton)v.findViewById(R.id.imageButtonDownComment);
        imageButtonCommentaireSuivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexcommentaires++;
                if(indexcommentaires > listeCommentairesUtilisateurs.get(listeCommentairesUtilisateurs.keySet().toArray()[indexListeLieuxCommentes]).size()-1)
                    indexcommentaires=0;
                setFields();
            }
        });

        imageButtonPreviousSpot = (ImageButton)v.findViewById(R.id.imageButtonPreviousSpot);
        imageButtonPreviousSpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(_typelieu.equals("lieuxcréés")) {
                    indexListeLieuxCrees--;
                    if(indexListeLieuxCrees<0)
                        indexListeLieuxCrees = listeSpotsCrees.size() - 1;
                }
                else if (_typelieu.equals("lieuxcommentés")) {
                    indexListeLieuxCommentes--;
                    indexcommentaires = 0;
                    if(indexListeLieuxCommentes<0)
                        indexListeLieuxCommentes = listeCommentairesUtilisateurs.keySet().size() - 1;
                }else if (_typelieu.equals("totalcommentaires")) {
                    indexTotalCommentaires--;
                    if(indexTotalCommentaires<0)
                        indexTotalCommentaires = listeAllComments.size() - 1;
                }
                setFields();
            }
        });

        imageButtonNextSpot = (ImageButton)v.findViewById(R.id.imageButtonNextSpot);
        imageButtonNextSpot.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                if(_typelieu.equals("lieuxcréés")) {
                    indexListeLieuxCrees++;
                    if(indexListeLieuxCrees> listeSpotsCrees.size() -1)
                        indexListeLieuxCrees = 0;
                }
                else if (_typelieu.equals("lieuxcommentés")) {
                    indexListeLieuxCommentes++;
                    indexcommentaires = 0;
                    if(indexListeLieuxCommentes> listeCommentairesUtilisateurs.keySet().size() - 1)
                        indexListeLieuxCommentes = 0;

                }else if (_typelieu.equals("totalcommentaires")) {

                    indexTotalCommentaires++;
                    if(indexTotalCommentaires > listeAllComments.size() - 1)
                        indexTotalCommentaires = 0;
                }
                setFields();
            }
        });

        imageButtonFirstSpot = (ImageButton)v.findViewById(R.id.imageButtonFirstSpot);
        imageButtonFirstSpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(_typelieu.equals("lieuxcréés"))
                    indexListeLieuxCrees = 0;
                else if (_typelieu.equals("lieuxcommentés")) {
                    indexListeLieuxCommentes = 0;
                    indexcommentaires = 0;
                }else if (_typelieu.equals("totalcommentaires"))
                    indexTotalCommentaires = 0;
                setFields();
            }
        });

        imageButtonLastSpot = (ImageButton)v.findViewById(R.id.imageButtonLastSpot);
        imageButtonLastSpot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(_typelieu.equals("lieuxcréés")) {
                    indexListeLieuxCrees = listeSpotsCrees.size()- 1;

                }else if (_typelieu.equals("lieuxcommentés")) {
                    indexcommentaires = 0;
                    indexListeLieuxCommentes = listeCommentairesUtilisateurs.keySet().size() - 1;
                }else if (_typelieu.equals("totalcommentaires"))
                    indexTotalCommentaires = listeAllComments.size() - 1;

                setFields();
            }
        });

        imageButtonCancelModifDeleteComment = (ImageButton)v.findViewById(R.id.imageButtonCancelModifDeleteComment);
        imageButtonCancelModifDeleteComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Confirmer la suppression de votre commentaire ?")
                        .setCancelable(false)
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {


                                Commentaire com = null;
                                int idcomment = -1;
                                Spot s = null;
                                if (_typelieu.equals("lieuxcommentés")) {
                                    s = (Spot) listeCommentairesUtilisateurs.keySet().toArray()[indexListeLieuxCommentes];
                                    idcomment = listeCommentairesUtilisateurs.get(s).get(indexcommentaires).getId();
                                }else if (_typelieu.equals("totalcommentaires"))
                                {
                                    idcomment = listeAllComments.get(indexTotalCommentaires).getId();
                                    s = listeAllComments.get(indexTotalCommentaires).getSpot();
                                }

                                new DeleteCommentTask(s, id,  UrbanFoodApplication.getCurrentUser().getMail()).execute();
                            }
                        })
                        .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.show();


            }
        });

        imageButtonUpdateComment = (ImageButton)v.findViewById(R.id.imageButtonUpdateComment);

        imageButtonUpdateComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Confirmer la mise à jour de votre commentaire ?")
                        .setCancelable(false)
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                int idcomment = -1;
                                Commentaire com = null;
                                if (_typelieu.equals("lieuxcommentés")) {
                                    Spot s = (Spot) listeCommentairesUtilisateurs.keySet().toArray()[indexListeLieuxCommentes];
                                    com = listeCommentairesUtilisateurs.get(s).get(indexcommentaires);
                                    idcomment = listeCommentairesUtilisateurs.get(s).get(indexcommentaires).getId();
                                }else if (_typelieu.equals("totalcommentaires")) {
                                    com = listeAllComments.get(indexTotalCommentaires);
                                    idcomment = listeAllComments.get(indexTotalCommentaires).getId();
                                }
                                new UpdateCommentTask(com, idcomment, UrbanFoodApplication.getCurrentUser().getMail(), edtTextCommentaire.getText().toString()).execute();
                            }
                        })
                        .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.show();



            }
        });

        return v;
    }

    public void setCurrentItem (int itemindex, boolean smoothScroll) {
        ((MonCompteActivity)getActivity()).getPager().setCurrentItem(itemindex, smoothScroll);
    }

    private ViewGroup initViewsMonCompte(ViewGroup v) {
        listeSpotsCommentes = new ArrayList<Spot>();
        txtNbreLieuxCrees = (TextView) v.findViewById(R.id.textViewNombreLieuxCrees);
        txtNbreLieuxCommentes = (TextView) v.findViewById(R.id.textViewNombreLieuxCommentes);
        txtLibActivite = (TextView)v.findViewById(R.id.textViewLibActivite);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        edtLogin = (EditText) v.findViewById(R.id.edtLogin);
        edtPassword = (EditText) v.findViewById(R.id.edtMotDePasse);
        edtMail = (EditText) v.findViewById(R.id.edtMail);
        edtMail.setFocusable(false);

        txtDateCreationCompte = (TextView) v.findViewById(R.id.textDateCreation);
        Polices.setContext(getContext());
        txtDateCreationCompte.setTypeface(Polices.TrouverPolices("Akbaal"));
        txtLibActivite.setTypeface(Polices.TrouverPolices("Akbaal"));

        edtMail.setText(UrbanFoodApplication.getCurrentUser().getMail());
        edtLogin.setText(UrbanFoodApplication.getCurrentUser().getLogin());
        edtPassword.setText(UrbanFoodApplication.getCurrentUser().getPassword());


        txtDateCreationCompte.setText("Membre depuis le : "+ UrbanFoodApplication.getCurrentUser().getDateFormatted());

        new GetListeSpotsUtilisateurData("Commentés").execute();
        new GetListeSpotsUtilisateurData("Créés").execute();

        btnSupprimerCompte = (Button) v.findViewById(R.id.btnSupprimerCompte);
        btnSupprimerCompte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowConfirmSuppression();
            }
        });

        btnUpdateUser = (Button)v.findViewById(R.id.buttonEditUtilisateur);
        btnUpdateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new UpdateUserTask(edtLogin.getText().toString(), edtMail.getText().toString(), UrbanFoodApplication.getCurrentUser().getPassword(),edtPassword.getText().toString()).execute();

            }
        });

        edtLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().equals(UrbanFoodApplication.getCurrentUser().getLogin()) && edtPassword.getText().toString().equals(UrbanFoodApplication.getCurrentUser().getPassword()))
                    btnUpdateUser.setVisibility(View.GONE);
                else
                    btnUpdateUser.setVisibility(View.VISIBLE);
            }
        });

        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().equals(UrbanFoodApplication.getCurrentUser().getPassword()) && edtLogin.getText().toString().equals(UrbanFoodApplication.getCurrentUser().getLogin()))
                    btnUpdateUser.setVisibility(View.GONE);
                else
                    btnUpdateUser.setVisibility(View.VISIBLE);
            }
        });

        txtTotalCommentairesUtilisateur = (TextView) v.findViewById(R.id.textViewTotalCommentaires);

        btnVisuNbreLieuxCrees = (Button)v.findViewById(R.id.btnNbreLieuxCrees);
        btnVisuNbreLieuxCrees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCurrentItem(1,true);
                _typelieu = "lieuxcréés";
                mMap.clear();
                AjouterMakersLieuxCrees(listeSpotsCrees);
                setFields();

            }
        });

        btnVisuNbreTotalCommentaires = (Button)v.findViewById(R.id.btnNbreTotalCommentaires);
        btnVisuNbreTotalCommentaires.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _typelieu = "totalcommentaires";
                setCurrentItem(1,true);
                AjouterMakersAllComments(listeAllComments);
                setFields();
            }
        });

        btnNbreLieuxCommentes = (Button)v.findViewById(R.id.btnNbreLieuxCommentes);
        btnNbreLieuxCommentes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _typelieu = "lieuxcommentés";
                setCurrentItem(1,true);
                AjouterMakersLieuxCommentes(listeAllComments);

                setFields();
            }
        });

        return v;
    }

    private void AjouterMakersLieuxCommentes(ArrayList<Commentaire> listeAllComments) {
        mMap.clear();
        for ( Object spot : listeCommentairesUtilisateurs.keySet().toArray() ) {
            Spot s = (Spot)spot;
            mMap.addMarker(new MarkerOptions().position(new LatLng(s.getLatitude(), s.getLongitude())).icon(BitmapDescriptorFactory.fromBitmap(getMarkerIcon(s.getTypeLieu()))).title(s.getNomLieu()));

        }
    }

    private void AjouterMakersAllComments(ArrayList<Commentaire> listeAllComments) {
        mMap.clear();
        for (Commentaire com : listeAllComments)
        {
            mMap.addMarker(new MarkerOptions().position(new LatLng(com.getSpot().getLatitude(), com.getSpot().getLongitude())).icon(BitmapDescriptorFactory.fromBitmap(getMarkerIcon(com.getSpot().getTypeLieu()))).title(com.getSpot().getNomLieu()));

        }
    }

    private void AjouterMakersLieuxCrees(ArrayList<Spot> listeSpotsCrees) {
        mMap.clear();
        for (Spot s :
                listeSpotsCrees) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(s.getLatitude(), s.getLongitude())).icon(BitmapDescriptorFactory.fromBitmap(getMarkerIcon(s.getTypeLieu()))).title(s.getNomLieu()));

        }
    }

    private void setFields() {

        if(_typelieu.equals("lieuxcréés")) {

            txtNomSpot.setText(listeSpotsCrees.get(indexListeLieuxCrees).getNomLieu());
            txtDescriptionSpot.setText(listeSpotsCrees.get(indexListeLieuxCrees).getDescriptionLieu());
            txtLatitudeSpot.setText("Lat. : "+ String.valueOf(listeSpotsCrees.get(indexListeLieuxCrees).getLatitude()));
            txtLongitudeSpot.setText("Long. " + String.valueOf(listeSpotsCrees.get(indexListeLieuxCrees).getLongitude()));
            txtIndexMap.setText((indexListeLieuxCrees + 1) + "/" + listeSpotsCrees.size());
            txtViewUtilisateur.setVisibility(View.VISIBLE);
            txtViewUtilisateur.setText("Vous avez identifié ce lieu le " + listeSpotsCrees.get(indexListeLieuxCrees).getDateFormatted());
            layoutNombreCommentaires.setVisibility(View.GONE);
            layoutControlesCommentaires.setVisibility(View.GONE);
            setHeader(listeSpotsCrees.get(indexListeLieuxCrees));

            Spot spot = listeSpotsCrees.get(indexListeLieuxCrees);
            currentSpot = spot;

            //mMap.addMarker(new MarkerOptions().position(new LatLng(spot.getLatitude(), spot.getLongitude())).icon(BitmapDescriptorFactory.fromBitmap(getMarkerIcon(spot.getTypeLieu()))).title(spot.getNomLieu()));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(spot.getLatitude(), spot.getLongitude()), 12.0f));

        }
        else if(_typelieu.equals("totalcommentaires"))
        {
            Spot spot = (Spot) listeAllComments.get(indexTotalCommentaires).getSpot();
            currentSpot = spot;

            layoutControlesCommentaires.setVisibility(View.VISIBLE);
            layoutcontrolesDefilementCommentaires.setVisibility(View.GONE);

            txtNomSpot.setText(spot.getNomLieu());
            txtDescriptionSpot.setText(spot.getDescriptionLieu());
            txtLatitudeSpot.setText("Lat. : "+ spot.getLatitude());
            txtLongitudeSpot.setText("Long. " + spot.getLongitude());
            txtIndexMap.setText((indexTotalCommentaires+ 1) + "/" + listeAllComments.size());

            if(listeAllComments.get(indexTotalCommentaires).getDateModificationFormatted()!="")
                txtViewUtilisateur.setText("Modifié le " + listeAllComments.get(indexTotalCommentaires).getDateModificationFormatted());
            else
                txtViewUtilisateur.setText("Ecrit le " + listeAllComments.get(indexTotalCommentaires).getDateFormatted());
            txtViewUtilisateur.setVisibility(View.VISIBLE);

            layoutNombreCommentaires.setVisibility(View.VISIBLE);
            textWatcher.setEnabled(false);
            edtTextCommentaire.setText(listeAllComments.get(indexTotalCommentaires).getTexte());
            textWatcher.setEnabled(false);
            textViewNombreCommentaires.setText(String.valueOf(listeCommentairesUtilisateurs.get(spot).size()));
            setHeader(spot);

            //mMap.addMarker(new MarkerOptions().position(new LatLng(spot.getLatitude(), spot.getLongitude())).icon(BitmapDescriptorFactory.fromBitmap(getMarkerIcon(spot.getTypeLieu()))).title(spot.getNomLieu()));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(spot.getLatitude(), spot.getLongitude()), 12.0f));


        }
        else if(_typelieu.equals("lieuxcommentés")) {

            Spot spot = (Spot) listeCommentairesUtilisateurs.keySet().toArray()[indexListeLieuxCommentes];
            currentSpot = spot;
            ArrayList<Commentaire> listComSpot = listeCommentairesUtilisateurs.get(spot);
            layoutControlesCommentaires.setVisibility(View.VISIBLE);
            if(listComSpot.size()> 1)
                layoutcontrolesDefilementCommentaires.setVisibility(View.VISIBLE);
            else
                layoutcontrolesDefilementCommentaires.setVisibility(View.GONE);
            txtNomSpot.setText(spot.getNomLieu());
            txtDescriptionSpot.setText(spot.getDescriptionLieu());
            txtLatitudeSpot.setText("Lat. : "+ spot.getLatitude());
            txtLongitudeSpot.setText("Long. " + spot.getLongitude());
            txtIndexMap.setText((indexListeLieuxCommentes + 1) + "/" + listeCommentairesUtilisateurs.keySet().size());

            if(listeCommentairesUtilisateurs.get(spot).get(indexcommentaires).getDateModificationFormatted()!="")
                txtViewUtilisateur.setText("Modifié le " + listeCommentairesUtilisateurs.get(spot).get(indexcommentaires).getDateModificationFormatted());
            else
                txtViewUtilisateur.setText("Ecrit le " + listeCommentairesUtilisateurs.get(spot).get(indexcommentaires).getDateFormatted());

            txtViewUtilisateur.setVisibility(View.VISIBLE);

            layoutNombreCommentaires.setVisibility(View.VISIBLE);
            textWatcher.setEnabled(false);
            edtTextCommentaire.setText(listeCommentairesUtilisateurs.get(spot).get(indexcommentaires).getTexte());
            textWatcher.setEnabled(true);
            textViewNombreCommentaires.setText((indexcommentaires + 1) + "/" + listComSpot.size());

            setHeader(spot);

            //mMap.addMarker(new MarkerOptions().position(new LatLng(spot.getLatitude(), spot.getLongitude())).icon(BitmapDescriptorFactory.fromBitmap(getMarkerIcon(spot.getTypeLieu()))).title(spot.getNomLieu()));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(spot.getLatitude(), spot.getLongitude()), 12.0f));

        }

    }

    private Bitmap getMarkerIcon(String type)
    {
        Bitmap bmp = null;
        if(type.equals("Potagers"))
            bmp = BitmapFactory.decodeResource(getResources(),R.mipmap.location_potager);
        else if(type.equals("Sources"))
            bmp = BitmapFactory.decodeResource(getResources(),R.mipmap.location_source);
        else if(type.equals("Invendus"))
            bmp = BitmapFactory.decodeResource(getResources(),R.mipmap.location_bread);
        else if(type.equals("Cueillette"))
            bmp = BitmapFactory.decodeResource(getResources(),R.mipmap.location_cueillette);

        return bmp;
    }

    private void setHeader(Spot spot) {
        if(spot.getTypeLieu().equals("Potagers")) {
            imgTypeSpot.setImageResource(R.mipmap.carrot_rounded);
            linearLayoutHeader.setBackgroundResource(R.drawable.layout_bg_potager);
        }
        else if(spot.getTypeLieu().equals("Sources"))
        {
            imgTypeSpot.setImageResource(R.mipmap.drop_rounded);
            linearLayoutHeader.setBackgroundResource(R.drawable.layout_bg_source);
        }
        else if(spot.getTypeLieu().equals("Invendus"))
        {
            imgTypeSpot.setImageResource(R.mipmap.bread_rounded);
            linearLayoutHeader.setBackgroundResource(R.drawable.layout_bg_invendus);
        }
        else if(spot.getTypeLieu().equals("Cueillette"))
        {
            imgTypeSpot.setImageResource(R.mipmap.cherry_rounded);
            linearLayoutHeader.setBackgroundResource(R.drawable.layout_bg_cueillette);
        }
    }

    public void ShowConfirmSuppression() {
        new AlertDialog.Builder(getContext())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.supprimer_compte_titre)
                .setMessage(R.string.supprimer_compte_texte)
                .setPositiveButton(R.string.confirmer, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new DeleteUserTask(UrbanFoodApplication.getCurrentUser().getMail(), UrbanFoodApplication.getCurrentUser().getPassword()).execute();

                    }

                })
                .setNegativeButton(R.string.annuler, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Stop the activity
                        Toast.makeText(getContext(), "Annulation de la fermeture", Toast.LENGTH_LONG).show();
                    }

                })
                .show();

    }

    public void setLibelleTotalCommentaire(int libelleTotalCommentaire) {

        txtTotalCommentairesUtilisateur.setText("Vous avez écrit " + String.valueOf(libelleTotalCommentaire) + " commentaires");
    }

    public void setListeCommentaires(Spot spot, ArrayList<Commentaire> result) {

        listeCommentairesUtilisateurs.put(spot, result);
        listeAllComments.addAll(result);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        setMap(googleMap);
    }

    private static void setMap(GoogleMap googleMap) {
        mMap = googleMap;
    }


    class DeleteCommentTask extends AsyncTask<Object, Void, Boolean>
    {

        private ProgressDialog progressDialog;

        String _mail;
        String _commentaire;
        int _comdcode;
        Spot _spot;


        public DeleteCommentTask(Spot spot, int comcode,  String mail) {

            this._mail = mail;
            this._comdcode = comcode;
            this._spot = spot;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Suppression en cours...");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected Boolean doInBackground(Object[] objects) {
            return getWebServiceDeleteCommentaireResponse(_comdcode, _mail);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (progressDialog.isShowing())
                progressDialog.dismiss();

            if(result){
                Toast.makeText(getContext(),"Le commentaire a bien été supprimé.",Toast.LENGTH_LONG).show();
                if (_typelieu.equals("lieuxcommentés")) {
                    listeCommentairesUtilisateurs.get(_spot).remove(indexcommentaires);
                }else if (_typelieu.equals("totalcommentaires"))
                {
                    listeAllComments.remove(indexTotalCommentaires);
                }

            }
            else
            {
                Toast.makeText(getContext(),"Erreur rencontrée lors du traitement de votre requête de suppression. Contactez un administrateur afin qu'il prenne en charge ce problème.",Toast.LENGTH_LONG).show();
            }
        }

        protected Boolean getWebServiceDeleteCommentaireResponse(int id,  String mail) {

            try {

                url = new URL(SERVICES_PATH + "SupprimerCommentaire?&comcode="+ id+"&mail=" + mail);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");

                int responseCode = conn.getResponseCode();

                Log.d("TAG", "Response code: " + responseCode);
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    // Reading response from input Stream
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(conn.getInputStream()));
                    String output;
                    response = new StringBuffer();

                    while ((output = in.readLine()) != null) {
                        response.append(output);
                    }
                    in.close();
                }}
            catch(Exception e){
                e.printStackTrace();
            }
            Boolean result = true;
            responseText = response.toString();
            //Call ServerData() method to call webservice and store result in response
            //  response = service.ServerData(path, postDataParams);
            result = Boolean.valueOf(responseText);

            return result;
        }
    }

    class UpdateCommentTask extends AsyncTask<Object, Void, Boolean>
    {

        private ProgressDialog progressDialog;

        String _mail;
        String _commentaire;
        int _comdcode;
        Commentaire _com;


        public UpdateCommentTask(Commentaire com, int comcode,  String mail,  String commentaire) {

            this._com = com;
            this._mail = mail;
            this._commentaire = commentaire;
            this._comdcode = comcode;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Mise à jour en cours...");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected Boolean doInBackground(Object[] objects) {
            return getWebServiceUpdateCommentaireResponse(_comdcode, _mail, _commentaire);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (progressDialog.isShowing())
                progressDialog.dismiss();

            if(result){
                Toast.makeText(getContext(),"Modifications enregistrées.",Toast.LENGTH_LONG).show();
                _com.setTexte(_commentaire);
            }
            else
            {
                Toast.makeText(getContext(),"Erreur rencontrée lors du traitement de votre requête de modification. Contactez un administrateur afin qu'il prenne en charge ce problème.",Toast.LENGTH_LONG).show();
            }
        }

        protected Boolean getWebServiceUpdateCommentaireResponse(int id,  String mail, String commentaire) {

            try {

                url = new URL(SERVICES_PATH + "ModifierCommentaire?&comcode="+ id+"&mail=" +mail+ "&commentaire="+(commentaire.replaceAll(" ", "%20").replaceAll("'", "%27")));

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");

                int responseCode = conn.getResponseCode();

                Log.d("TAG", "Response code: " + responseCode);
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    // Reading response from input Stream
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(conn.getInputStream()));
                    String output;
                    response = new StringBuffer();

                    while ((output = in.readLine()) != null) {
                        response.append(output);
                    }
                    in.close();
                }}
            catch(Exception e){
                e.printStackTrace();
            }
            Boolean result = true;
            responseText = response.toString();
            //Call ServerData() method to call webservice and store result in response
            //  response = service.ServerData(path, postDataParams);
            result = Boolean.valueOf(responseText);

            return result;
        }
    }




    class UpdateUserTask extends AsyncTask<Object, Void, Boolean>
    {

        private ProgressDialog progressDialog;
        String _login;
        String _mail;
        String _password;
        String _newpassword;


        public UpdateUserTask(String login, String mail, String password, String nouveaupassword) {
            this._login = login;
            this._mail = mail;
            this._password = password;
            this._newpassword = nouveaupassword;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Mise à jour de vos paramètres en cours...");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected Boolean doInBackground(Object[] objects) {
            return getWebServiceUpdateUserResponse(_login, _mail, _password, _newpassword);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (progressDialog.isShowing())
                progressDialog.dismiss();

            if(result){
                Toast.makeText(getContext(),"Modifications enregistrées.",Toast.LENGTH_LONG).show();

            }
            else
            {
                Toast.makeText(getContext(),"Erreur rencontrée lors du traitement de votre requête de modification. Contactez un administrateur afin qu'il prenne en charge ce problème.",Toast.LENGTH_LONG).show();
            }
        }
    }

    protected Boolean getWebServiceUpdateUserResponse(String login,String mail, String password, String nouveaupassword) {

        try {

            url = new URL(SERVICES_PATH + "ModifierUtilisateur?&mail="+ mail+"&password=" +password+"&newpassword="+nouveaupassword+ "&login="+login );

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();

            Log.d("TAG", "Response code: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                // Reading response from input Stream
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String output;
                response = new StringBuffer();

                while ((output = in.readLine()) != null) {
                    response.append(output);
                }
                in.close();
            }}
        catch(Exception e){
            e.printStackTrace();
        }
        Boolean result = true;
        responseText = response.toString();
        //Call ServerData() method to call webservice and store result in response
        //  response = service.ServerData(path, postDataParams);
        result = Boolean.valueOf(responseText);

        return result;
    }


    class DeleteUserTask extends AsyncTask<Object, Void, Boolean>
    {

        private ProgressDialog progressDialog;
        String _mail;
        String _password;

        public DeleteUserTask(String mail, String password) {
            this._mail = mail;
            this._password = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Suppression en cours...");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected Boolean doInBackground(Object[] objects) {
            return getWebServiceDeleteUserResponse(_mail, _password);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (progressDialog.isShowing())
                progressDialog.dismiss();

            if(!result){
                Toast.makeText(getContext(),"Votre compte a bien été supprimé. Vous allez etre rédirigé",Toast.LENGTH_LONG).show();
                getActivity().finish();
                Intent it = new Intent(getContext(), LoginActivity.class);
                startActivity(it);
            }
            else
            {
                Toast.makeText(getContext(),"Le traitement de suppression a échoué. Contactez un administrateur afin qu'il prenne en charge ce problème.",Toast.LENGTH_LONG).show();
            }




        }
    }

    protected Boolean getWebServiceDeleteUserResponse(String mail, String password) {

        try {

            url = new URL(SERVICES_PATH + "SupprimerUtilisateur?mail="+ mail+"&password=" +password );

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();

            Log.d("TAG", "Response code: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                // Reading response from input Stream
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String output;
                response = new StringBuffer();

                while ((output = in.readLine()) != null) {
                    response.append(output);
                }
                in.close();
            }}
        catch(Exception e){
            e.printStackTrace();
        }
        Boolean result = true;
        responseText = response.toString();
        //Call ServerData() method to call webservice and store result in response
        //  response = service.ServerData(path, postDataParams);
        result = Boolean.valueOf(responseText);

        return result;
    }


    class GetListeSpotsUtilisateurData extends AsyncTask<Object, Spot, ArrayList<Spot>>
    {

        private ProgressDialog progressDialog;
        String _type;
        private int itotalcommentaire;

        public GetListeSpotsUtilisateurData(String type) {
            this._type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Chargement des informations...");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected ArrayList<Spot> doInBackground(Object[] objects) {
            return getWebServiceResponseData(_type);
        }


        protected ArrayList<Spot> getWebServiceResponseData(String type) {

            try {
                if(type.equals("Créés"))
                    url = new URL(SERVICES_PATH + "ListeSpotsCreesUtilisateur?mail="+ UrbanFoodApplication.getCurrentUser().getMail());
                else if(type.equals("Commentés"))
                    url = new URL(SERVICES_PATH + "ListeSpotsCommentesUtilisateur?mail=" + UrbanFoodApplication.getCurrentUser().getMail());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");

                int responseCode = conn.getResponseCode();

                Log.d("TAG", "Response code: " + responseCode);
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    // Reading response from input Stream
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(conn.getInputStream()));
                    String output;
                    response = new StringBuffer();

                    while ((output = in.readLine()) != null) {
                        response.append(output);
                    }
                    in.close();
                }}
            catch(Exception e){
                e.printStackTrace();
            }
            ArrayList<Spot> spots = new ArrayList<Spot>();
            responseText = response.toString();
            //Call ServerData() method to call webservice and store result in response
            //  response = service.ServerData(path, postDataParams);
            itotalcommentaire = 0;
            Log.d("TAG", "data:" + responseText);
            try {
                JSONArray jsonarray = new JSONArray(responseText);
                for (int i = 0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);

                    Spot s = new Spot();

                    s.setDateCreation(jsonobject.getString("DateCreation"));
                    s.setLatitude(jsonobject.getDouble("Latitude"));
                    s.setLongitude(jsonobject.getDouble("Longitude"));
                    s.setNomLieu(jsonobject.getString("LieuNom"));
                    s.setUtilisateur(jsonobject.getString("Utilisateur"));
                    s.setDescriptionLieu(jsonobject.getString("Description"));
                    s.setIdLieu(jsonobject.getInt("LieuCode"));
                    s.setTypeLieu(jsonobject.getString("Type"));
                    s.setNombreCommentaires(jsonobject.getInt("NombreCommentaires"));
                    s.setVille(jsonobject.getString("Ville"));

                    if(type.equals("Commentés")) {
                        itotalcommentaire+=s.getNombreCommentaires();
                        publishProgress(s);
                    }
                    spots.add(s);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return spots;
        }

        @Override
        protected void onProgressUpdate(Spot... values) {
            notifySpotAdded(values[0]);

        }

        @Override
        protected void onPostExecute(ArrayList<Spot> result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            if(_type.equals("Commentés")) {
                listeSpotsCommentes = result;
                setLibelleTotalCommentaire(itotalcommentaire);
            }else if(_type.equals("Créés"))
                listeSpotsCrees = result;


            setLibelleNombreLieux(_type, result.size());

        }
    }

    private void notifySpotAdded(Spot value) {
        new GetListeCommentairesData(value, edtMail.getText().toString(), this).execute();
    }


    private void setLibelleNombreLieux(String type, int size) {
        if(type.equals("Créés")) {
            txtNbreLieuxCrees.setText("Vous avez identifié " + String.valueOf(size) + " lieux");
            if(size>0)
                btnVisuNbreLieuxCrees.setVisibility(View.VISIBLE);
        }else if(type.equals("Commentés")) {
            txtNbreLieuxCommentes.setText("Vous avez commentés " + String.valueOf(size) + " lieux");
            if(size>0)
            {
                btnNbreLieuxCommentes.setVisibility(View.VISIBLE);
                btnVisuNbreTotalCommentaires.setVisibility(View.VISIBLE);
            }

        }
    }


}
