package adapters;

import classes.Utilisateur;

/**
 * Created by Administrateur on 13/09/2017.
 */

public class UrbanFoodApplication  {

    private static Utilisateur _currentUser;


    public static Utilisateur getCurrentUser() {
        return _currentUser;
    }

    public static void setCurrentUser(Utilisateur _currentUser) {
        UrbanFoodApplication._currentUser = _currentUser;
    }
}
