package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.urbanfood.urbanfood.R;

import java.util.ArrayList;

import classes.Commentaire;

/**
 * Created by Spoon on 14/09/2017.
 */



public class ListeCommentairesAdapter extends ArrayAdapter<Commentaire> {

    private ArrayList<Commentaire> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtDate;
        TextView txtCommment;


    }

    public ListeCommentairesAdapter(ArrayList<Commentaire> data, Context context) {
        super(context, R.layout.comment_item, data);
        this.dataSet = data;
        this.mContext=context;

    }



    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Commentaire dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.comment_item, parent, false);
            viewHolder.txtDate = (TextView) convertView.findViewById(R.id.txtDate);
            viewHolder.txtCommment = (TextView) convertView.findViewById(R.id.txtContent);



            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }



        if(!dataModel.getDateModification().equals(""))
            viewHolder.txtDate.setText("Modifié le " + dataModel.getDateFormatted() + ", par " + dataModel.getNomUtilisateur());
        else
            viewHolder.txtDate.setText("Rédigé le " + dataModel.getDateFormatted() + ", par " + dataModel.getNomUtilisateur());

        viewHolder.txtCommment.setText(dataModel.getTexte());

        // Return the completed view to render on screen
        return convertView;
    }
}