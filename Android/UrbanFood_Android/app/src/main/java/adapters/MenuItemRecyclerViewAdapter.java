package adapters;

/**
 * Created by Spoon on 07/09/2017.
 */


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;

import com.urbanfood.urbanfood.R;

import java.util.List;

import classes.MenuItem;

public class MenuItemRecyclerViewAdapter  extends RecyclerView.Adapter<MenuItemViewHolders> {

    private List<MenuItem> itemList;
    private Context context;

    public MenuItemRecyclerViewAdapter(Context context, List<MenuItem> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public MenuItemViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.menuitemlayout, null);
        MenuItemViewHolders rcv = new MenuItemViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolders holder, int position) {
        holder.libelle.setText(itemList.get(position).getLibelle());
        holder.image.setImageResource(itemList.get(position).getImage());
        holder.conteneur.setBackgroundColor(Color.parseColor(itemList.get(position).getBackgroundHexaColor()));
        holder.cardview.setTag(itemList.get(position).getLibelle());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}