USE [master]
GO
/****** Object:  Database [UrbanFruit]    Script Date: 05/09/2017 10:57:49 ******/
CREATE DATABASE [UrbanFruit]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UrbanFood', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\UrbanFood.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'UrbanFood_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\UrbanFood_log.ldf' , SIZE = 10176KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [UrbanFruit] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UrbanFruit].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [UrbanFruit] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [UrbanFruit] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [UrbanFruit] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [UrbanFruit] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [UrbanFruit] SET ARITHABORT OFF 
GO
ALTER DATABASE [UrbanFruit] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [UrbanFruit] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [UrbanFruit] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [UrbanFruit] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [UrbanFruit] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [UrbanFruit] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [UrbanFruit] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [UrbanFruit] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [UrbanFruit] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [UrbanFruit] SET  DISABLE_BROKER 
GO
ALTER DATABASE [UrbanFruit] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [UrbanFruit] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [UrbanFruit] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [UrbanFruit] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [UrbanFruit] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [UrbanFruit] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [UrbanFruit] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [UrbanFruit] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [UrbanFruit] SET  MULTI_USER 
GO
ALTER DATABASE [UrbanFruit] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [UrbanFruit] SET DB_CHAINING OFF 
GO
ALTER DATABASE [UrbanFruit] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [UrbanFruit] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [UrbanFruit]
GO
/****** Object:  User [thomas]    Script Date: 05/09/2017 10:57:50 ******/
CREATE USER [thomas] FOR LOGIN [thomas] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [marine]    Script Date: 05/09/2017 10:57:50 ******/
CREATE USER [marine] FOR LOGIN [marine] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [jerome]    Script Date: 05/09/2017 10:57:50 ******/
CREATE USER [jerome] FOR LOGIN [jerome] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [eric]    Script Date: 05/09/2017 10:57:50 ******/
CREATE USER [eric] FOR LOGIN [eric] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [elisabeth]    Script Date: 05/09/2017 10:57:51 ******/
CREATE USER [elisabeth] FOR LOGIN [elisabeth] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [thomas]
GO
ALTER ROLE [db_owner] ADD MEMBER [marine]
GO
ALTER ROLE [db_owner] ADD MEMBER [jerome]
GO
ALTER ROLE [db_owner] ADD MEMBER [eric]
GO
ALTER ROLE [db_owner] ADD MEMBER [elisabeth]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CalculerDistanceLatLng]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_CalculerDistanceLatLng](
@latitude float,
@longitude float,
@latitude2 float,
@longitude2 float)

returns float
as 
begin
declare @lon float
declare @lat float
declare @lon2 float
declare @lat2 float

declare @a float
declare @distance float
declare @radius float
set @radius = 6371.0E

set @lon = RADIANS(@longitude)
set @lat = RADIANS(@latitude)
set @lon2 = RADIANS(@longitude2)
set @lat2 = RADIANS(@latitude2)

set @a = SQRT(square(sin((@lat2 - @lat)/2.0E)) + 
			(cos(@lat) * cos(@lat2) * square(sin((@lon2 - @lon)/2.0E))))


set @distance =
				@radius *( 2.0E * asin(case when 1.0E < @a then 1.0E else @a end))

return @distance
end

GO
/****** Object:  UserDefinedFunction [dbo].[fn_getIdType]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_getIdType](
@type nvarchar(100))
returns int
as
begin 
	
	return (select typ_code FROM TYPES WHERE typ_nom = @type)
	 
end

GO
/****** Object:  Table [dbo].[commentaire]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[commentaire](
	[com_code] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[uti_code] [numeric](18, 0) NOT NULL,
	[lie_code] [numeric](18, 0) NOT NULL,
	[com_contenu] [text] NOT NULL,
	[com_datecrea] [datetime] NOT NULL,
	[com_actif] [bit] NOT NULL,
 CONSTRAINT [PK_commentaire] PRIMARY KEY NONCLUSTERED 
(
	[com_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


/****** Object:  Table [dbo].[etre_interesse]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[etre_interesse](
	[uti_code] [numeric](18, 0) NOT NULL,
	[vil_code] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_etre_interesse] PRIMARY KEY CLUSTERED 
(
	[uti_code] ASC,
	[vil_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[lieu]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lieu](
	[lie_code] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[uti_code] [numeric](18, 0) NOT NULL,
	[typ_code] [numeric](18, 0) NOT NULL,
	[vil_code] [numeric](18, 0) NOT NULL,
	[lie_nom] [varchar](30) NOT NULL,
	[lie_description] [text] NULL,
	[lie_datecrea] [datetime] NOT NULL,
	[lie_latitude] [numeric](15, 6) NULL,
	[lie_longitude] [numeric](15, 6) NULL,
 CONSTRAINT [PK_lieu] PRIMARY KEY NONCLUSTERED 
(
	[lie_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TYPES]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[type](
	[typ_code] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[typ_nom] [varchar](25) NOT NULL,
 CONSTRAINT [PK_type] PRIMARY KEY NONCLUSTERED 
(
	[typ_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[utilisateur]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[utilisateur](
	[uti_code] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[uti_nom] [varchar](30) NOT NULL,
	[uti_email] [varchar](50) NULL,
	[uti_mdp] [varchar](100) NOT NULL,
	[uti_datecrea] [datetime] NULL,
 CONSTRAINT [PK_utilisateur] PRIMARY KEY NONCLUSTERED 
(
	[uti_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[ville]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ville](
	[vil_code] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vil_nom] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ville] PRIMARY KEY NONCLUSTERED 
(
	[vil_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[AFFICHAGE]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*select * from ville*/

create view [dbo].[AFFICHAGE] as 
select vil_nom, lie_nom, lie_description, lie_latitude, lie_longitude 
from lieu inner join ville on lieu.vil_code=ville.vil_code

/*select * from TYPES*/
GO
/****** Object:  Index [COMMENTER_FK]    Script Date: 05/09/2017 10:57:51 ******/
CREATE NONCLUSTERED INDEX [COMMENTER_FK] ON [dbo].[commentaire]
(
	[lie_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ECRIRE_FK]    Script Date: 05/09/2017 10:57:51 ******/
CREATE NONCLUSTERED INDEX [ECRIRE_FK] ON [dbo].[commentaire]
(
	[uti_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [etre_interesse2_FK]    Script Date: 05/09/2017 10:57:51 ******/
CREATE NONCLUSTERED INDEX [etre_interesse2_FK] ON [dbo].[etre_interesse]
(
	[vil_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CREER_FK]    Script Date: 05/09/2017 10:57:51 ******/
CREATE NONCLUSTERED INDEX [CREER_FK] ON [dbo].[lieu]
(
	[uti_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ETRE_TYPE_FK]    Script Date: 05/09/2017 10:57:51 ******/
CREATE NONCLUSTERED INDEX [ETRE_TYPE_FK] ON [dbo].[lieu]
(
	[typ_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [SE_TROUVER_DANS_FK]    Script Date: 05/09/2017 10:57:51 ******/
CREATE NONCLUSTERED INDEX [SE_TROUVER_DANS_FK] ON [dbo].[lieu]
(
	[vil_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [NonClusteredIndex-20170727-230948]    Script Date: 05/09/2017 10:57:51 ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170727-230948] ON [dbo].[TYPES]
(
	[typ_nom] ASC,
	[typ_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [NonClusteredIndex-20170727-230250]    Script Date: 05/09/2017 10:57:51 ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170727-230250] ON [dbo].[ville]
(
	[VIL_NOM] ASC,
	[vil_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[commentaire]  WITH CHECK ADD  CONSTRAINT [FK_COMMENTA_COMMENTER_lieu] FOREIGN KEY([lie_code])
REFERENCES [dbo].[lieu] ([lie_code])
GO
ALTER TABLE [dbo].[commentaire] CHECK CONSTRAINT [FK_COMMENTA_COMMENTER_lieu]
GO
ALTER TABLE [dbo].[commentaire]  WITH CHECK ADD  CONSTRAINT [FK_COMMENTA_ECRIRE_UTILISAT] FOREIGN KEY([uti_code])
REFERENCES [dbo].[utilisateur] ([uti_code])
GO
ALTER TABLE [dbo].[commentaire] CHECK CONSTRAINT [FK_COMMENTA_ECRIRE_UTILISAT]
GO
ALTER TABLE [dbo].[etre_interesse]  WITH CHECK ADD  CONSTRAINT [FK_ETRE_INT_ETRE_INTE_UTILISAT] FOREIGN KEY([uti_code])
REFERENCES [dbo].[utilisateur] ([uti_code])
GO
ALTER TABLE [dbo].[etre_interesse] CHECK CONSTRAINT [FK_ETRE_INT_ETRE_INTE_UTILISAT]
GO
ALTER TABLE [dbo].[etre_interesse]  WITH CHECK ADD  CONSTRAINT [FK_ETRE_INT_ETRE_INTE_ville] FOREIGN KEY([vil_code])
REFERENCES [dbo].[ville] ([vil_code])
GO
ALTER TABLE [dbo].[etre_interesse] CHECK CONSTRAINT [FK_ETRE_INT_ETRE_INTE_ville]
GO
ALTER TABLE [dbo].[lieu]  WITH CHECK ADD  CONSTRAINT [FK_lieu_CREER_UTILISAT] FOREIGN KEY([uti_code])
REFERENCES [dbo].[utilisateur] ([uti_code])
GO
ALTER TABLE [dbo].[lieu] CHECK CONSTRAINT [FK_lieu_CREER_UTILISAT]
GO
ALTER TABLE [dbo].[lieu]  WITH CHECK ADD  CONSTRAINT [FK_lieu_ETRE_TYPE_TYPES] FOREIGN KEY([typ_code])
REFERENCES [dbo].[TYPES] ([typ_code])
GO
ALTER TABLE [dbo].[lieu] CHECK CONSTRAINT [FK_lieu_ETRE_TYPE_TYPES]
GO
ALTER TABLE [dbo].[lieu]  WITH CHECK ADD  CONSTRAINT [FK_lieu_SE_TROUVE_ville] FOREIGN KEY([vil_code])
REFERENCES [dbo].[ville] ([vil_code])
GO
ALTER TABLE [dbo].[lieu] CHECK CONSTRAINT [FK_lieu_SE_TROUVE_ville]
GO
/****** Object:  StoredProcedure [dbo].[enregistrementLieux]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[enregistrementLieux] 
@uticode int,
@typcode int, 
@vilcode int, 
@lieunom nvarchar(30),
@lieudescription text,
@lat decimal(9,6),
@long decimal(9,6)

as 

insert into lieu values (@uticode, @typcode, @vilcode, @lieunom, @lieudescription, getdate(), @lat, @long) 

GO
/****** Object:  StoredProcedure [dbo].[Liste_users]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Liste_users]
as
select uti_nom from utilisateur

GO
/****** Object:  StoredProcedure [dbo].[sp_lieu_Liste]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_lieu_Liste] 
@type nvarchar(100) = 'Tous',
@latitude decimal(9,6) = -1,
@longitude decimal(9,6) = -1, 
@radius int = 5

as 
	-- a. tous les arbres
	if(@type = 'Tous')
	begin
		if((@latitude = -1) and (@longitude = -1))
			select Li.lie_code, lie_datecrea, 
			Li.lie_description, lie_latitude, lie_longitude, 
			typ_nom, uti_nom,
			VIL_NOM, lie_datecrea
			from lieu Li INNER JOIN TYPES Ty on Li.typ_code = Ty.typ_code
			INNER JOIN utilisateur Uti ON Li.uti_code = Uti.uti_code
			INNER JOIN ville Vi ON Li.vil_code = Vi.vil_code
		else
			select Li.lie_code, lie_datecrea, 
			Li.lie_description, lie_latitude, lie_longitude, 
			typ_nom, uti_nom,
			VIL_NOM, lie_datecrea from lieu
			Li INNER JOIN TYPES Ty on Li.typ_code = Ty.typ_code
			INNER JOIN utilisateur Uti ON Li.uti_code = Uti.uti_code
			INNER JOIN ville Vi ON Li.vil_code = Vi.vil_code
			WHERE (select [dbo].[fn_CalculerDistanceLatLng](@latitude, @longitude, Li.lie_latitude, Li.lie_longitude)) <= @radius
	end
	--b. Seulement les arbres du type passé en paramètre @type
	else
	begin
		declare @idtype bigint
		set @idtype = (select dbo.fn_getIdType(@type))
		if((@latitude = -1) and (@longitude = -1))
			--.1 Pas de latitude/longitude => tous les arbres du types sans tenir compte de la distance et de la localisation
			select Li.lie_code, lie_datecrea, 
			Li.lie_description, lie_latitude, lie_longitude, 
			typ_nom, uti_nom,
			VIL_NOM, lie_datecrea from lieu Li INNER JOIN TYPES Ty on Li.typ_code = Ty.typ_code
			INNER JOIN utilisateur Uti ON Li.uti_code = Uti.uti_code 
			INNER JOIN ville Vi ON Li.vil_code = Vi.vil_code
			WHERE Ty.typ_code = @idtype
		else
			--2. Avec latitude et longitude => tous les arbres du types @type dans un rayon de @radius kilometres
			-- autour du point passé en paramètre 
			select Li.lie_code, lie_datecrea, 
			Li.lie_description, lie_latitude, lie_longitude, 
			typ_nom, uti_nom,
			VIL_NOM, lie_datecrea from lieu
			Li INNER JOIN TYPES Ty on Li.typ_code = Ty.typ_code
			INNER JOIN utilisateur Uti ON Li.uti_code = Uti.uti_code
			INNER JOIN ville Vi ON Li.vil_code = Vi.vil_code
			WHERE ((select [dbo].[fn_CalculerDistanceLatLng](@latitude, @longitude, Li.lie_latitude, Li.lie_longitude)) <= @radius)
			AND Ty.typ_code = @idtype
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_TrouverUtilisateur]    Script Date: 05/09/2017 10:57:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script de la commande SelectTopNRows à partir de SSMS  ******/
create procedure [dbo].[sp_TrouverUtilisateur]
@mail nvarchar(250)
as
SELECT * FROM utilisateur 
where uti_email=@mail;
GO
USE [master]
GO
ALTER DATABASE [UrbanFruit] SET  READ_WRITE 
GO
