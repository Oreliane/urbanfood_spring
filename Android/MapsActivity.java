package com.urbanfood.urbanfood;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import adapters.MenuItemRecyclerViewAdapter;
import classes.MenuItem;
import classes.Spot;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String CODE_HEXA_FOND_COMPTE = "#c3c2c5";
    private static final String CODE_HEXA_FOND_BOULANGERIE = "#fdae01";
    private static final String CODE_HEXA_FOND_POTAGER = "#4c9e0d";
    private static final String CODE_HEXA_FOND_SOURCE = "#1976D2";
    private static final String CODE_HEXA_FOND_OPTIONS =  "#636f73";
    private static final String CODE_HEXA_FOND_TOUS = "#832133";
    private static final String CODE_HEXA_FOND_APROPOS = "#603f6a";
    private static final String CODE_HEXA_FOND_DECONNEXION = "#8dbad8";
    private static final String CODE_HEXA_FOND_CUEILLETTE = "#ff9a7a";


    private GoogleMap mMap;

    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_maps);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);

        List<MenuItem> gaggeredList = null;
        // a remplacer par mode invité : utiliser enum
        Boolean b = false;
        if(b)
            gaggeredList = getListMenuItemsGuest();
        else
          gaggeredList = getListMenuItems();


        MenuItemRecyclerViewAdapter rcAdapter = new MenuItemRecyclerViewAdapter(MapsActivity.this, gaggeredList);
        recyclerView.setAdapter(rcAdapter);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapFragment.getView().setVisibility(View.GONE);
    }


    private List<MenuItem> getListMenuItemsGuest() {
        List<MenuItem> listViewItems = new ArrayList<MenuItem>();
        listViewItems.add(new MenuItem("Potagers", R.drawable.carrot_64, CODE_HEXA_FOND_POTAGER));
        listViewItems.add(new MenuItem("Cueillette", R.drawable.cherry_64, CODE_HEXA_FOND_CUEILLETTE));
        listViewItems.add(new MenuItem("Sources", R.drawable.drop_64, CODE_HEXA_FOND_SOURCE));
        listViewItems.add(new MenuItem("Invendus", R.drawable.bread_64, CODE_HEXA_FOND_BOULANGERIE));
        listViewItems.add(new MenuItem("Tous", R.drawable.tous_64, CODE_HEXA_FOND_TOUS));
        listViewItems.add(new MenuItem("A propos", R.drawable.settings_64, CODE_HEXA_FOND_OPTIONS));
        listViewItems.add(new MenuItem("Quitter", R.drawable.logout_64, CODE_HEXA_FOND_DECONNEXION));
        return listViewItems;
    }

    private List<MenuItem> getListMenuItems() {
        List<MenuItem> listViewItems = new ArrayList<MenuItem>();
        listViewItems.add(new MenuItem("Potagers", R.drawable.carrot_64, CODE_HEXA_FOND_POTAGER));
        listViewItems.add(new MenuItem("Cueillette", R.drawable.cherry_64, CODE_HEXA_FOND_CUEILLETTE));
        listViewItems.add(new MenuItem("Sources", R.drawable.drop_64, CODE_HEXA_FOND_SOURCE));
        listViewItems.add(new MenuItem("Invendus", R.drawable.bread_64, CODE_HEXA_FOND_BOULANGERIE));
        listViewItems.add(new MenuItem("Tous", R.drawable.tous_64, CODE_HEXA_FOND_TOUS));
        listViewItems.add(new MenuItem("Mon Compte", R.drawable.user_64, CODE_HEXA_FOND_COMPTE));
        listViewItems.add(new MenuItem("A propos", R.drawable.about_64, CODE_HEXA_FOND_APROPOS));
        //listViewItems.add(new MenuItem("Options", R.drawable.settings_64, CODE_HEXA_FOND_OPTIONS));
        listViewItems.add(new MenuItem("Deconnexion", R.drawable.logout_64, CODE_HEXA_FOND_DECONNEXION));
        return listViewItems;
    }

    private List<Spot> getListSpot(){

        List<Spot> l = new ArrayList<Spot>();
        Spot spot1 = new Spot(8,1,"Cueillette",45712,"Cerisiers","Cerisiers en accès libre", "11-09-2017", 47.416518, 0.755884);
        Spot spot2 = new Spot(11,1,"Potagers",16384,"Potager collaboratif","Légumes et plantes aromatiques en cueillette libre", "05-09-2017",47.387177,0.702211);
        Spot spot3 = new Spot(12,1,"Sources",16384,"Point d'eau","Fontaine d'eau potable en accès libre","2017-09-05 10:08:34.580",	47.387177, 0.702211);
        Spot spot4 = new Spot(13,1,"Sources",16384,"Point d'eau Bergeonnerie","Fontaine d'eau potable en accès libre","2017-09-05 10:10:10.360",47.366960,0.697951);
        Spot spot5 = new Spot(14,1,"Cueillette",45391,"Châtaigniers","Chataigniers de petite et grande taille, nombreux fruits","2017-09-05 10:12:24.233",	47.228674, 0.375839);
        Spot spot6 = new Spot(15,1,"Sources",16384,"Point d'eau Radegonde","Fontaine d'eau potable en accès libre, Parc de Sainte Radegonde","2017-09-05 10:17:52.730",47.401793,0.706655);
        Spot spot7 = new Spot(17,1,"Sources",16384,"Point d'eau Chateaubriand","Fontaine d'eau potable, Jardin de Chateaubriand","2017-09-05 10:20:05.770",47.421806,0.682270);
        Spot spot8 = new Spot(18,1,"Cueillette",47831,"Pommes","Gros pommier","2017-09-05 10:20:07.937",47.451739,0.713253);
        Spot spot9 = new Spot(32,1,"Sources",16384,"Point d'eau Balzac","Fontaine d'eau potable, Parc Honoré de Balzac","2017-09-05 10:49:11.783",47.371682,0.704664);
        Spot spot10 = new Spot(22,1,"Sources",16384,"Point d'eau Beaux Arts","Fontaine d'eau potable, Jardin du Musée des Beaux Arts","2017-09-05 10:35:39.790",47.394661,0.693267);
        Spot spot11 = new Spot(23,1,"Sources",16384,"Point d'eau Prébendes","Fontaine d'eau potable, Jardin des Prébendes d'Oé","2017-09-05 10:38:47.733",47.385268,0.684693);
        Spot spot12 = new Spot(24,1,"Sources",16384,"Point d'eau Botannique","Fontaine d'eau potable, Jardin Botannique","2017-09-05 10:39:33.030",47.384892,0.667551);
        Spot spot13 = new Spot(25,1,"Sources",16384,"Point d'eau Boylesve","Fontaine d'eau potable, Jardin René Boylesve","2017-09-05 10:40:38.717",47.382111,0.683992);
        Spot spot14 = new Spot(26,1,"Sources",16384,"Point d'eau Theuriet","Fontaine d'eau potable, Jardin André Theuriet","2017-09-05 10:41:23.777",47.383107,0.698273);
        Spot spot15 = new Spot(27,1,"Sources",16384,"Point d'eau Rotonde","Fontaine d'eau potable, Jardin de la Rotonde","2017-09-05 10:43:17.307",47.380636,0.699957);
        Spot spot16 = new Spot(28,1,"Sources",16384,"Point d'eau Mazoué","Fontaine d'eau potable, Square Jean Mazoué (école Pauline Kergomard)","2017-09-05 10:45:40.043",47.380196,0.703696);
        Spot spot17 = new Spot(29,1,"Sources",16384,"Point d'eau Meffre","Fontaine d'eau potable, Jardin Meffre","2017-09-05 10:46:39.850",47.378821,0.699710);
        Spot spot18 = new Spot(33,1,"Sources",16384,"Point d'eau Mirabeau","Fontaine d'eau potable, Jardin Mirabeau","2017-09-05 10:54:13.400",47.393413,0.698935);
        Spot spot19 = new Spot(34,1,"Potagers",45712,"Potager collaboratif Ruche","Potager collaboratif de la Ruche qui dit oui, jardins de Terre Exotique","2017-09-05 14:01:02.943",47.409442,0.761165);
        Spot spot20 = new Spot(19,1,"Cueillette",41838,"Cerises","Belles cerises","2017-09-05 10:25:14.820",47.503328,0.782003);
        Spot spot21 = new Spot(21,1,"Cueillette",45713,"Patates","Champ de patates","2017-09-05 10:34:52.963",47.370321,0.750933);

        l.add(spot1);
        l.add(spot2);
        l.add(spot3);
        l.add(spot4);
        l.add(spot5);
        l.add(spot6);
        l.add(spot7);
        l.add(spot8);
        l.add(spot9);
        l.add(spot10);
        l.add(spot11);
        l.add(spot12);
        l.add(spot13);
        l.add(spot14);
        l.add(spot15);
        l.add(spot16);
        l.add(spot17);
        l.add(spot18);
        l.add(spot19);
        l.add(spot20);
        l.add(spot21);

        return l;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
