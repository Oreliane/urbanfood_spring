package com.urbanfood.urbanfood;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import adapters.MenuItemRecyclerViewAdapter;
import classes.MenuItem;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String CODE_HEXA_FOND_COMPTE = "#f1f1f1";
    private static final String CODE_HEXA_FOND_BOULANGERIE = "#fdae01";
    private static final String CODE_HEXA_FOND_POTAGER = "#4c9e0d";
    private static final String CODE_HEXA_FOND_SOURCE = "#1976D2";
    private static final String CODE_HEXA_FOND_OPTIONS =  "#636f73";
    private static final String CODE_HEXA_FOND_TOUS = "#832133";
    private static final String CODE_HEXA_FOND_APROPOS = "#603f6a";
    private static final String CODE_HEXA_FOND_DECONNEXION = "#472316";


    private GoogleMap mMap;

    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_maps);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);

        List<MenuItem> gaggeredList = null;
        // a remplacer par mode invité : utiliser enum
        Boolean b = false;
        if(b)
            gaggeredList = getListMenuItemsGuest();
        else
          gaggeredList = getListMenuItems();


        MenuItemRecyclerViewAdapter rcAdapter = new MenuItemRecyclerViewAdapter(MapsActivity.this, gaggeredList);
        recyclerView.setAdapter(rcAdapter);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapFragment.getView().setVisibility(View.GONE);
    }


    private List<MenuItem> getListMenuItemsGuest() {
        List<MenuItem> listViewItems = new ArrayList<MenuItem>();
        listViewItems.add(new MenuItem("Potagers", R.drawable.carrot_64, CODE_HEXA_FOND_POTAGER));
        listViewItems.add(new MenuItem("Sources", R.drawable.drop_64, CODE_HEXA_FOND_SOURCE));
        listViewItems.add(new MenuItem("Invendus", R.drawable.bread_64, CODE_HEXA_FOND_BOULANGERIE));
        listViewItems.add(new MenuItem("Tous", R.drawable.tous_64, CODE_HEXA_FOND_TOUS));
        listViewItems.add(new MenuItem("A propos", R.drawable.settings_64, CODE_HEXA_FOND_OPTIONS));
        listViewItems.add(new MenuItem("Quitter", R.drawable.logout_64, CODE_HEXA_FOND_DECONNEXION));
        return listViewItems;
    }

    private List<MenuItem> getListMenuItems() {
        List<MenuItem> listViewItems = new ArrayList<MenuItem>();
        listViewItems.add(new MenuItem("Potagers", R.drawable.carrot_64, CODE_HEXA_FOND_POTAGER));
        listViewItems.add(new MenuItem("Sources", R.drawable.drop_64, CODE_HEXA_FOND_SOURCE));
        listViewItems.add(new MenuItem("Invendus", R.drawable.bread_64, CODE_HEXA_FOND_BOULANGERIE));
        listViewItems.add(new MenuItem("Tous", R.drawable.tous_64, CODE_HEXA_FOND_TOUS));
        listViewItems.add(new MenuItem("Mon Compte", R.drawable.user_64, CODE_HEXA_FOND_COMPTE));
        listViewItems.add(new MenuItem("A propos", R.drawable.about_64, CODE_HEXA_FOND_APROPOS));
        listViewItems.add(new MenuItem("Options", R.drawable.settings_64, CODE_HEXA_FOND_OPTIONS));
        listViewItems.add(new MenuItem("Deconnexion", R.drawable.logout_64, CODE_HEXA_FOND_DECONNEXION));
        return listViewItems;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
