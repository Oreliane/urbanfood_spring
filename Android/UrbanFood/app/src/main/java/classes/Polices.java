package classes;

/**
 * Created by Spoon on 07/09/2017.
 */



import android.content.Context;
import android.graphics.Typeface;

import com.urbanfood.urbanfood.R;


public class Polices {


    static String[] fontassets = new String[]{"Defaut", "A_Bit_Empty.ttf", "a_dripping_marker.ttf", "A_Font_with_Serifs.ttf", "a_yummy_apology.ttf", "Aaargh.ttf",
            "Abberancy.ttf", "Abbeyline.ttf", "Aberration.ttf", "Aerobus Dotty.ttf", "Aguafina Script.ttf", "akbaal.ttf", "Alex Brush.ttf",
            "Allura.ttf", "AlumFreePromotional.ttf", "amadeus.ttf", "Ampad Brush.ttf", "Amperzand.ttf", "Android.ttf", "Angelina.ttf", "Anglican.ttf",
            "Arabella.ttf",  "Arizonia.ttf", "Armalite Rifle.ttf", "Asenine.ttf",
            "AstronBoyWonder.ttf", "betonia freeshape.ttf", "blowbrush.ttf",
            "Cairotiqua Freestyle.ttf", "carbon phyber.ttf", "Chantelli_Antiqua.ttf", "Corleone.ttf",
            "FatFineFree.ttf", "Freebooter Script.ttf", "Freelance Kamchatka.ttf", "FreeStyle.ttf", "Fresco Stamp.ttf", "Ghost Of The Wild West.ttf", "Hollywood.ttf",
            "Jesaya Free.ttf", "Philosopher.ttf", "Plain Old Handwriting.ttf", "princeofpersia.ttf", "shanghai.ttf",
            "Street Freehand.ttf", "Sunset.ttf", "Transformers_Movie.ttf", "Ulse Freehand.ttf", "zekton free.ttf"};

    private static Context _context;

    public static String[] TableauPolices() {
        return fontassets;
    }

    public static Typeface  TrouverPolices(String nompolice) {

        if(nompolice.equals("Defaut")) {
            Typeface face = Typeface.DEFAULT;
            return face;
        }
        String[] strPolicesArray = _context.getResources().getStringArray(R.array.PolicesName_array);
        for (int i = 0; i< strPolicesArray.length - 1; i ++)

            if((nompolice).equals(strPolicesArray[i]))
            {
                String fontpath = "fonts/" + fontassets[i];
                Typeface face = Typeface.createFromAsset(_context.getResources().getAssets(), fontpath);
                return face;
            }


        return null;
    }

    public static void setContext(Context context)
    {
        _context = context;
    }


}
