package classes;

/**
 * Created by Spoon on 07/09/2017.
 */

public class MenuItem {
    private String _libelle;
    private int _image;
    private String _hexacolor;

    /***
     *
     * @param lib String : Libelle du menuitem
     * @param icon int : Correspond à la ressource rpojet de l'image à afficher (ex. R.drawable.bread_64)
     * @param hexacolorbackground: String Code hexadécimal de la couleur de fond du menuitem
     */
    public MenuItem(String lib, int icon, String hexacolorbackground) {
        this._libelle = lib;
        this._image = icon;
        this._hexacolor = hexacolorbackground;
    }


    public String getLibelle() {
        return _libelle;
    }

    public void setLibelle(String libelle) {
        this._libelle = libelle;
    }

    public int getImage() {
        return _image;
    }


    public void setImage(int image) {
        this._image = image;
    }

    public String getBackgroundHexaColor()
    {
        return this._hexacolor;
    }

    public String setBackgroundHexaColor(String hexacolor)
    {
        return this._hexacolor;
    }
}
