package adapters;

/**
 * Created by Spoon on 07/09/2017.
 */

import android.support.v7.widget.RecyclerView;
        import android.view.View;
        import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
        import android.widget.Toast;

import com.urbanfood.urbanfood.R;

public class MenuItemViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView libelle;
    public ImageView image;
    public RelativeLayout conteneur;

    public MenuItemViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        libelle = (TextView) itemView.findViewById(R.id.libelle_menuitem);
        image = (ImageView) itemView.findViewById(R.id.logo_menuitem);
        conteneur = (RelativeLayout) itemView.findViewById(R.id.conteneur_menuitem);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}