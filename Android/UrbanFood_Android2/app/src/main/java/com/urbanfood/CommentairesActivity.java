package com.urbanfood;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.urbanfood.urbanfood.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import AsyncTasks.GetListeCommentairesData;
import adapters.ListeCommentairesAdapter;
import adapters.UrbanFoodApplication;
import classes.Commentaire;
import classes.Spot;

public class CommentairesActivity extends AppCompatActivity {

    private int idspot;
    private URL url;
    private StringBuffer response;
    private String responseText;


    final String SERVICES_PATH_ENREGISTRERCOMMENTAIRE = "http://88.169.107.232:8888/SowizServicesHost/SowizServices.svc/EnregistrerCommentaire?";
    private ArrayList<Commentaire> listeCommentaires;
    private ListView listViewCommentaires;
    private ImageButton btnAjoutCommentaire;
    private EditText edtTextCommentaire;
    private TextView txtViewNoComment;
    private ListeCommentairesAdapter listeCommentairesAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commentaires);
        Bundle b =getIntent().getExtras();
        idspot = b.getInt("idspot");
        listeCommentaires = new ArrayList<Commentaire>();
        new GetListeCommentairesData(idspot, this).execute();
        listViewCommentaires = (ListView)findViewById(R.id.ListeCommentaires);
        btnAjoutCommentaire = (ImageButton)findViewById(R.id.AjouterCommentaires);
        edtTextCommentaire = (EditText)findViewById(R.id.edtTextCommentaire);
        txtViewNoComment = (TextView)findViewById(R.id.textViewNoComment);
        btnAjoutCommentaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtTextCommentaire.getText().length()>0)
                new InsertCommentTask(idspot, edtTextCommentaire.getText().toString()).execute();
            }
        });
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void setListeCommentaires(ArrayList<Commentaire> result) {
        listeCommentaires = result;
        if(listeCommentaires.size()> 0 )
        {
            listeCommentairesAdapter = new ListeCommentairesAdapter(listeCommentaires, getApplicationContext());
            listViewCommentaires.setAdapter(listeCommentairesAdapter);
            txtViewNoComment.setVisibility(View.GONE);
        }else
            txtViewNoComment.setVisibility(View.GONE);
    }


    class InsertCommentTask extends AsyncTask<Object, Void, Commentaire>
    {

        private final String _text;
        private ProgressDialog progressDialog;
        int _idspot;

        public InsertCommentTask(int id, String text) {
            this._idspot = id;
            this._text = text.replaceAll(" ", "%20").replaceAll("'", "%27");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressDialog = new ProgressDialog(CommentairesActivity.this);
            progressDialog.setMessage("Enregistrement du commentaire...");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected Commentaire doInBackground(Object[] objects) {
            return getWebServiceInsertCommentResponseData(_idspot, _text);
        }

        @Override
        protected void onPostExecute(Commentaire result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            listeCommentairesAdapter.add(result);
            listeCommentairesAdapter.notifyDataSetChanged();
            if(result.getId()!= -1)
                edtTextCommentaire.setText("");

        }
    }

    protected Commentaire getWebServiceInsertCommentResponseData(int idspot, String text) {

        try {

            url = new URL(SERVICES_PATH_ENREGISTRERCOMMENTAIRE +"lieucode="+ idspot + "&mail=" + UrbanFoodApplication.getCurrentUser().getMail() + "&commentaire=" + text);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();

            Log.d("TAG", "Response code: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                // Reading response from input Stream
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String output;
                response = new StringBuffer();

                while ((output = in.readLine()) != null) {
                    response.append(output);
                }
                in.close();
            }}
        catch(Exception e){
            e.printStackTrace();
        }
        Commentaire comment = new Commentaire();
        responseText = response.toString();
        //Call ServerData() method to call webservice and store result in response
        //  response = service.ServerData(path, postDataParams);
        Log.d("TAG", "data:" + responseText);
        try {

                JSONObject jsonobject = new JSONObject(responseText);
                comment.setDate(jsonobject.getString("Date"));
                comment.setId(jsonobject.getInt("Id"));

                comment.setMailUtilisateur(jsonobject.getString("MailUtilisateur"));
                comment.setCodeUtilisateur(jsonobject.getInt("CodeUtilisateur"));
                comment.setNomUtilisateur(jsonobject.getString("NomUtilisateur"));

                if(!jsonobject.getString("DateModification").equals("null"))
                    comment.setDateModification(jsonobject.getString("DateModification"));

                comment.setTexte(jsonobject.getString("Text"));


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return comment;
    }


}
