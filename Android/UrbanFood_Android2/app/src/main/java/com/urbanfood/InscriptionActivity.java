package com.urbanfood;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.urbanfood.urbanfood.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import classes.Polices;
import classes.Utilisateur;

public class InscriptionActivity extends AppCompatActivity {


    private static final String SERVICES_PATH = "http://88.169.107.232:8888/SowizServicesHost/SowizServices.svc/EnregistrerUtilisateur?";
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mLoginView;
    private View mProgressView;
    private View mLoginFormView;
    private TextView txtAppName;

    private UserCreateTask mAuthTask = null;
    private URL url;
    private StringBuffer response;
    private EditText mConfirmPasswordView;
    private TextView mTitreView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mTitreView = (TextView) findViewById(R.id.TitreInscription);
        Polices.setContext(getApplicationContext());
        mTitreView.setTypeface(Polices.TrouverPolices("Akbaal"));

        mPasswordView = (EditText) findViewById(R.id.password);
        mConfirmPasswordView = (EditText) findViewById(R.id.confirm_password);
        mLoginView = (EditText) findViewById(R.id.login);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.inscription_form);
        mProgressView = findViewById(R.id.inscription_progress);
    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String login = mLoginView.getText().toString();
        String confirmpassword = mConfirmPasswordView.getText().toString();
        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password))
        {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }
        else if(!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(login))
        {
            mLoginView.setError(getString(R.string.error_field_required));
            focusView = mLoginView;
            cancel = true;
        }
        else if(login.length()<4) {
            mLoginView.setError("Le login saisi fait moins de 4 caractères");
            focusView = mLoginView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if(TextUtils.isEmpty(confirmpassword)) {
            mConfirmPasswordView.setError(getString(R.string.error_field_required));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        else if(!confirmpassword.equals(password)) {
            mConfirmPasswordView.setError("Les champs mot de passe et sa confirmation sont différents");
            focusView = mConfirmPasswordView;
            cancel = true;
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserCreateTask(login, email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 3;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    protected Utilisateur getWebServiceResponseData(String mLogin,String mEmail, String mPassword) {

        try {

            url = new URL(SERVICES_PATH + "login=" + mLogin + "&mail="+ mEmail +"&password=" + mPassword);
            Log.d("TAG", "ServerData: " + SERVICES_PATH);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();

            Log.d("TAG", "Response code: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                // Reading response from input Stream
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String output;
                response = new StringBuffer();

                while ((output = in.readLine()) != null) {
                    response.append(output);
                }
                in.close();
            }}
        catch(Exception e){
            e.printStackTrace();
        }

        String responseText = response.toString();
        Utilisateur util = null;
        //  response = service.ServerData(path, postDataParams);
        Log.d("TAG", "data:" + responseText);
        try {
            util = new Utilisateur();
            JSONObject obj = new JSONObject(responseText);
            util.setLogin(obj.getString("Login"));
            util.setMail(obj.getString("Mail"));
            util.setPassword(obj.getString("Password"));
            util.setId(obj.getInt("CodeUtilisateur"));


            if(obj.getInt("CodeUtilisateur")> 0)
            {

                String dateInString = obj.getString("DateCreationCompte");
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                Date date = null;
                try {

                    date = formatter.parse(dateInString);
                    System.out.println(date);
                    System.out.println(formatter.format(date));

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                util.setDateCreation(date);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return util;
    }

    public class UserCreateTask extends AsyncTask<Void, Void, Utilisateur> {

        private final String mEmail;
        private final String mLogin;
        private final String mPassword;
        private Context mContext;
        UserCreateTask(String login, String email, String password) {
            mEmail = email;
            mPassword = password;
            mLogin = login;

        }




        @Override
        protected Utilisateur doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            // Simulate network access.
            return getWebServiceResponseData(mLogin, mEmail, mPassword);


        }

        @Override
        protected void onPostExecute(final Utilisateur util) {
            mAuthTask = null;
            showProgress(false);

            if (util.getId()>0) {
                Toast.makeText(getApplicationContext(), "Votre compte a été créé avec succcès. Vous etes désormais mebre de la communauté Urban Food.", Toast.LENGTH_LONG).show();
                finish();

            } else {
                Toast.makeText(getApplicationContext(), "Echec de l'enregistrement.", Toast.LENGTH_LONG).show();
                setFocus(util.getId(), util.getMail());
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private void setFocus(int id, String libelle) {
        if(id ==-1) {
            mEmailView.requestFocus();
            mConfirmPasswordView.setError(libelle);
        }
    }
}
