package com.urbanfood;



import android.app.Presentation;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;


import com.urbanfood.urbanfood.R;

import java.util.ArrayList;
import java.util.List;

import anims.ZoomOutPageTransformer;
import classes.Polices;
import fragments.PresentationApplicationFragment;

public class AboutActivity extends AppCompatActivity {


    /**
     * Le view pager qui recoit les pages et gère la transition entre elles.
     */
    private ViewPager mPager;
    /**
     * Le pager adapter, qui fournit les pages au viewpager.
     */
    private PagerAdapter mPagerAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Bundle extras = getIntent().getExtras();
        Polices.setContext(getApplicationContext());
        initViews();


    }

    private void initViews() {
        // TODO Auto-generated method stub
        mPager = (ViewPager) findViewById(R.id.viewpagerAboutApplication);
        mPagerAdapter = new DescriptionApplicationsPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.clear();
        return true;
    }

    private class DescriptionApplicationsPagerAdapter extends FragmentStatePagerAdapter {
        public DescriptionApplicationsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            PresentationApplicationFragment fragment = new PresentationApplicationFragment();
            Bundle bundle = new Bundle(1);
            bundle.putInt("position", position);

            fragment.setArguments(bundle);
            return fragment;

        }

        @Override
        public int getCount() {
            return 5;
        }


    }

}
