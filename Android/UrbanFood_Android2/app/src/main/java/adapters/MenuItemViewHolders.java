package adapters;

/**
 * Created by Spoon on 07/09/2017.
 */

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
        import android.view.View;
        import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
        import android.widget.Toast;

import com.urbanfood.MapsActivity;
import com.urbanfood.urbanfood.R;

public class MenuItemViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView libelle;
    public ImageView image;
    public RelativeLayout conteneur;
    public CardView cardview;

    public MenuItemViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        libelle = (TextView) itemView.findViewById(R.id.libelle_menuitem);
        image = (ImageView) itemView.findViewById(R.id.logo_menuitem);
        conteneur = (RelativeLayout) itemView.findViewById(R.id.conteneur_menuitem);
        cardview = (CardView) itemView;
    }

    @Override
    public void onClick(View view) {
        String menuitem = (String) view.getTag();
        if(menuitem.equals("Potagers") || menuitem.equals("Sources") ||menuitem.equals("Invendus") ||menuitem.equals("Tous") || menuitem.equals("Cueillette"))
            ((MapsActivity)view.getContext()).AfficherCarte(menuitem);
        else if(menuitem.equals("Mon Compte"))
        {
            ((MapsActivity)view.getContext()).AfficherMonCompte();
        }
        else if(menuitem.equals("A propos"))
        {
            ((MapsActivity)view.getContext()).OuvrirAPropos();
        }
        else if(menuitem.equals("Deconnexion"))
        {
            ((MapsActivity)view.getContext()).ShowInviteFermeture();
        }
    }
}