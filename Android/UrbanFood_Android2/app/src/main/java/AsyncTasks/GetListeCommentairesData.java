package AsyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.urbanfood.CommentairesActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import adapters.ListeCommentairesAdapter;
import classes.Commentaire;
import classes.Spot;
import fragments.MonComptePagerFragment;

/**
 * Created by Spoon on 19/09/2017.
 */

public class GetListeCommentairesData extends AsyncTask<Object, Void, ArrayList<Commentaire>>
{

    private  Spot _spot;
    private  String _mailparam = "";
    private String _mail = "";
    private  CommentairesActivity _activity = null;
    private  MonComptePagerFragment _fragment = null;
    private ProgressDialog progressDialog;
    int _idspot;
    final String SERVICES_PATH_LISTECOMMENTAIRES = "http://88.169.107.232:8888/SowizServicesHost/SowizServices.svc/ListeCommentaires?";
    private StringBuffer response;
    private URL url;
    private String responseText;

    public GetListeCommentairesData(int id,  CommentairesActivity activity) {
        this._idspot = id;
        _activity = activity;

    }

    public GetListeCommentairesData(Spot spot, String mail, MonComptePagerFragment monComptePagerFragment) {
        this._spot = spot;
        this._idspot = spot.getIdLieu();
        this._fragment = monComptePagerFragment;
        _mail = mail;
        _mailparam = "&mail=" + mail;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Showing progress dialog
        Context c;
        if(_activity!= null)
            c = _activity;
        else
            c = _fragment.getContext();
        progressDialog = new ProgressDialog(c);
        progressDialog.setMessage("Chargement des commentaires...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    @Override
    protected ArrayList<Commentaire> doInBackground(Object[] objects) {
        return getWebServiceListeCommentairesResponseData(_idspot);
    }

    protected ArrayList<Commentaire> getWebServiceListeCommentairesResponseData(int idspot) {

        try {

            url = new URL(SERVICES_PATH_LISTECOMMENTAIRES +"idspot="+ idspot + _mailparam);


            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();

            Log.d("TAG", "Response code: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                // Reading response from input Stream
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String output;
                response = new StringBuffer();

                while ((output = in.readLine()) != null) {
                    response.append(output);
                }
                in.close();
            }}
        catch(Exception e){
            e.printStackTrace();
        }
        ArrayList<Commentaire> commentaires = new ArrayList<Commentaire>();
        responseText = response.toString();
        //Call ServerData() method to call webservice and store result in response
        //  response = service.ServerData(path, postDataParams);
        Log.d("TAG", "data:" + responseText);
        try {
            JSONArray jsonarray = new JSONArray(responseText);
            for (int i = 0; i < jsonarray.length(); i++) {
                JSONObject jsonobject = jsonarray.getJSONObject(i);

                Commentaire comment = new Commentaire();
                comment.setDate(jsonobject.getString("Date"));
                comment.setId(jsonobject.getInt("Id"));
                comment.setDateModification(jsonobject.getString("DateModification"));
                comment.setNomUtilisateur(jsonobject.getString("NomUtilisateur"));
                comment.setMailUtilisateur(jsonobject.getString("MailUtilisateur"));
                comment.setCodeUtilisateur(jsonobject.getInt("CodeUtilisateur"));
                comment.setTexte(jsonobject.getString("Text"));
                comment.setSpot(_spot);
                commentaires.add(comment);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return commentaires;
    }

    @Override
    protected void onPostExecute(ArrayList<Commentaire> result) {
        super.onPostExecute(result);

        // Dismiss the progress dialog
        if (progressDialog.isShowing())
            progressDialog.dismiss();
        if(_activity != null)
            _activity.setListeCommentaires(result);
        else
            _fragment.setListeCommentaires(_spot, result);

    }
}