package classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrateur on 11/09/2017.
 */

public class Commentaire {
    private String _date;
    private int _id;
    private String _nomutilisateur;
    private String _texte;
    private int _codeutilisateur;
    private String _mailutilisateur;
    private String _dateformatted;
    private String _datemodification;
    private String _datemodificationformatted;
    private Spot _spot;

    public void setDate(String date) {
        this._date = date;
        setDateFormatted(formateDate(_date));
    }

    public void setDateModification(String date) {
        this._datemodification = date;
        setDateModificationFormatted(formateDate(_datemodification));
    }

    public String getDateModification() {
        return this._datemodification;
    }

    private String formateDate(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = formatter.parse(date);
            formatter = new SimpleDateFormat("dd/MM/yyyy à HH:mm:ss");

            return formatter.format(d);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    public String getDate() {
        return this._date;
    }

    public void setId(int id) {
        this._id = id;
    }

    public int getId() {
        return this._id;
    }

    public void setNomUtilisateur(String nomutilisateur) {
        this._nomutilisateur = nomutilisateur;
    }

    public String getNomUtilisateur() {
        return this._nomutilisateur;
    }

    public void setTexte(String texte) {
        this._texte = texte;
    }

    public String getTexte() {
        return this._texte;
    }

    public void setCodeUtilisateur(int codeUtilisateur) {
        this._codeutilisateur = codeUtilisateur;
    }

    public int getCodeUtilisateur()
    {
        return this._codeutilisateur;
    }

    public void setMailUtilisateur(String mail) {
        this._mailutilisateur = mail;
    }

    public String getMailUtilisateur() {
        return this._mailutilisateur;
    }

    public String getDateFormatted() {
        return _dateformatted;
    }

    public void setDateFormatted(String dateformatted) {
        this._dateformatted = dateformatted;
    }

    public String getDateModificationFormatted() {
        return _datemodificationformatted;
    }

    public void setDateModificationFormatted(String dateformatted) {
        this._datemodificationformatted = dateformatted;
    }

    public void setSpot(Spot spot) {
        this._spot = spot;
    }

    public Spot getSpot()
    {
        return this._spot;
    }
}
