package fragments;


import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.urbanfood.urbanfood.R;

import classes.ImageHelper;
import classes.Polices;
import views.CircularImageView;


public class PresentationApplicationFragment extends Fragment {

    private static final String FONT = "Alex Brush";
    private ViewGroup rootView;

    public PresentationApplicationFragment() {
        // TODO Auto-generated constructor stub

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        int position = getArguments().getInt("position");


        rootView = initViews(position, inflater, container);
        return rootView;
    }



    private ViewGroup initViews(int position, LayoutInflater inflater, ViewGroup container) {
        // TODO Auto-generated method stub
        ViewGroup v = null;
        if(position ==0) {
            v = (ViewGroup) inflater.inflate(
                    R.layout.about_page_une, container, false);
            //v = initViewsPageUne(v);
        }else if (position ==1) {
            v = (ViewGroup) inflater.inflate(
                    R.layout.about_page_deux, container, false);
            //v = initViewsPageDeux(v);
        }
        else if (position ==2) {
            v = (ViewGroup) inflater.inflate(
                    R.layout.about_page_trois, container, false);
            //v = initViewsPageTrois(v);
        }
        else if (position == 3) {
            v = (ViewGroup) inflater.inflate(
                    R.layout.about_page_quatre, container, false);
            //v = initViewsPageQuatre(v);
        }
        else if (position == 4) {
            v = (ViewGroup) inflater.inflate(
                    R.layout.about_page_cinq, container, false);
            //v = initViewsPageQuatre(v);
        }
        return v;
    }



    private ViewGroup initViewsPageUne(ViewGroup v) {
        TextView txt = (TextView) v.findViewById(R.id.Titre);
        txt.setTypeface(Polices.TrouverPolices(FONT));

        //txt = (TextView) v.findViewById(R.id.sousTitre);
        txt.setTypeface(Polices.TrouverPolices(FONT));

        txt = (TextView) v.findViewById(R.id.Description);
        txt.setTypeface(Polices.TrouverPolices(FONT));

        //txt = (TextView) v.findViewById(R.id.Details);
        //txt.setTypeface(Polices.TrouverPolices(FONT));

        return v;
    }

    private ViewGroup initViewsPageDeux(ViewGroup v) {
        ImageView img = (ImageView) v.findViewById(R.id.imgPotager);
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.carrot_512);

        //img.setImageBitmap(bmp);
        return v;
    }

    private ViewGroup initViewsPageTrois(ViewGroup v) {
        return v;
    }

    private ViewGroup initViewsPageQuatre(ViewGroup v) {
        return v;
    }



}
