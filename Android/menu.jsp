<%@ page pageEncoding="UTF-8"%>


<!doctype html>
<html lang="en">
    <head>     
        <meta charset="utf-8" />
        <title>Urban Food : alimentation collaborative</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="js/main.js" type="text/javascript"></script>
        <link href="css/main.css" rel="stylesheet" type="text/css"/>
        <meta name="description"  content="" />     
    </head>
    <body id="home">
          
        <div id="page" style="height: 110px;">
              
        
            <button class="hamburger">
                <div class="hamburger__lines">
                    <div class="hamburger__line"></div>
                    <div class="hamburger__line"></div>
                    <div class="hamburger__line"></div>
                </div>
            </button>
             
            <div id="container" class="container">
                 
                <header id="header">
                    <nav>
                         
                        <ul id="primary" class="row">
                            <li class="accueil span2"><a title="Accueil" href="">Accueil</a>
                                <ul>
                                    <li><a href="">Dernières mises à jour</a></li>                                  
                                </ul>
                            </li>
                            <li class="cartes span2"><a  title="Cartes" href="">Cartes</a>
                                <ul>
                                    <li><a href="">Fruits sauvages</a></li>
                                    <li><a href="">Potagers collaboratifs</a></li>
                                    <li><a href="">Sources d'eau</a></li>
                                    <li><a href="">Invendus de boulangerie</a></li>
                                </ul>
                            </li>
                            <li class="blog span2"><a title="Accès utilisateur" href="">Connexion</a></li>
                            <li class="about span1"><a title="about" href="">A propos</a>
                                <ul>
                                    <li><a href="">Contact</a></li>
                                    <li><a href="">CGU</a></li>
                                </ul>
                            </li>
                            
                        </ul>			  		 

                    </nav>
                </header>                                
            </div>  
        </div>

        <script src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
                 
        <script src="js/main.js" type="text/javascript"></script>


    </body>
</html>


