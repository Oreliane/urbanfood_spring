package classes;

/**
 * Created by Administrateur on 11/09/2017.
 */

public class Spot {

    private int _idLieu=0;
    private int _iduser=0;
    private int _idVille=0;
    private String _typeLieu="";
    private String _nomLieu="";
    private String _descriptionLieu="";
    private String _dateCrea;
    private double _latitude=0;
    private double _longitude=0;

    public Spot(){
    }

    public Spot(int idLieu, int iduser, String typeLieu, int idVille, String nomLieu, String descriptionLieu,
                String dateCrea, double latitude, double longitude){

        this._idLieu= idLieu;
        this._iduser= iduser;
        this._typeLieu=typeLieu;
        this._idVille=idVille;
        this._nomLieu=nomLieu;
        this._descriptionLieu=descriptionLieu;
        this._dateCrea=dateCrea;
        this._latitude=latitude;
        this._longitude=longitude;

    }

    public int getIdLieu() {
        return _idLieu;
    }

    public void setIdLieu(int _idLieu) {
        this._idLieu = _idLieu;
    }

    public int getIduser() {
        return _iduser;
    }

    public void setIduser(int _iduser) {
        this._iduser = _iduser;
    }

    public String getTypeLieu() {
        return _typeLieu;
    }

    public void setTypeLieu(String _typeLieu) {
        this._typeLieu = _typeLieu;
    }

    public int getIdVille() {
        return _idVille;
    }

    public void setIdVille(int _idVille) {
        this._idVille = _idVille;
    }

    public String getNomLieu() {
        return _nomLieu;
    }

    public void setNomLieu(String _nomLieu) {
        this._nomLieu = _nomLieu;
    }

    public String getDescriptionLieu() {
        return _descriptionLieu;
    }

    public void setDescriptionLieu(String _descriptionLieu) {
        this._descriptionLieu = _descriptionLieu;
    }

    public String getDateCrea() {
        return _dateCrea;
    }

    public void setDateCrea(String _dateCrea) {
        this._dateCrea = _dateCrea;
    }

    public double getLatitude() {
        return _latitude;
    }

    public void setLatitude(double _latitude) {
        this._latitude = _latitude;
    }

    public double getLongitude() {
        return _longitude;
    }

    public void setLongitude(double _longitude) {
        this._longitude = _longitude;
    }
}
