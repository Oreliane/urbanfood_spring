package com.urban.sessions;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.context.WebApplicationContext;

import com.urban.entities.Utilisateur;

@SessionScope
@Component
public class SessionUtilisateur 
{
	
	public Utilisateur profile = new Utilisateur();
	
	public String debug = "coucou";
}
