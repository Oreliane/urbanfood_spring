package com.urban.web;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.urban.DAO.UtilisateurRepository;
import com.urban.entities.Utilisateur;

@Controller
public class UtilisateurController 
{
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	
	
	@RequestMapping(value = "/connexion", method = RequestMethod.POST)
	public String connexion(@RequestParam String email,@RequestParam String mdp, HttpSession session) {
		Utilisateur utilisateur = utilisateurRepository.findBySession(email, mdp);
		if (utilisateur != null) {
			session.setAttribute("utilisateur", utilisateur);
		} else {
			System.out.println("Vous n'existez pas !");
		}
		return "";		
	}
	
	@RequestMapping(value = "/uticreat", method = RequestMethod.POST)
	public String creation(HttpServletRequest req,@RequestParam String nom,@RequestParam String email, @RequestParam String mdp) 
	{	
		Utilisateur utilisateur = new Utilisateur();
		
		utilisateur.setUtiNom(nom);
		utilisateur.setUtiEmail(email);
		utilisateur.setUtiMdp(mdp);
		utilisateur.setUtiDatecrea(new Timestamp(System.currentTimeMillis()));
		
		utilisateurRepository.save(utilisateur);
		
		return "";
	}
	
	@RequestMapping(value = "/deconnexion", method = {RequestMethod.GET, RequestMethod.POST})
	public String creation(HttpSession session) 
	{	
		session.removeAttribute("utilisateur"); // pour etre sur
		session.invalidate(); 
		
		return "";
	}
}
