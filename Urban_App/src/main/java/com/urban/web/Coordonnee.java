package com.urban.web;

public class Coordonnee 
{
	private String libelle;
	private String latitude;
	private String longitude;
	private String description;
	private int code;
	
	
	
	public Coordonnee(int code, String libelle, String latitude, String longitude, String description) {
		super();
		this.code = code;
		this.libelle = libelle;
		this.latitude = latitude;
		this.longitude = longitude;
		this.description = description;
	}
	
	
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	
	
	
}
