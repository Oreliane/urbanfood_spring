package com.urban.web;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.urban.DAO.CommentaireRepository;
import com.urban.DAO.LieuRepository;
import com.urban.DAO.UtilisateurRepository;
import com.urban.entities.Commentaire;
import com.urban.entities.Lieu;
import com.urban.entities.Utilisateur;

@Controller
public class CommentaireController {
	private final String URL = "WEB-INF/consultationCommentaires.jsp";
	
	@Autowired
	private CommentaireRepository commentaireRepository;
	
	@Autowired
	private LieuRepository lieuRepository;
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@RequestMapping(value = "/InfoSpot", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView commentaire(ModelAndView mode, @RequestParam(required = true) int id, @RequestParam(required = false) String comment, HttpServletRequest request,HttpSession session) {
		
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
	
		Integer code = new Integer(id);
		Lieu lieu = lieuRepository.findOne(code);
		List<Commentaire> commentaire = commentaireRepository.findByLieu(lieu,true);
		
		
		mode.addObject("commentaires", commentaire);
		mode.addObject("lieu", lieu);
		mode.addObject("id", id);
		mode.addObject("sesssionUtilisateur", utilisateur);
		mode.setViewName(URL);	
		
		if (request.getMethod() == "POST" && !(comment.isEmpty())){

			Timestamp date = new Timestamp(System.currentTimeMillis());
			Commentaire ajoutComment = new Commentaire(true, comment, date, lieu, utilisateur);
			commentaireRepository.save(ajoutComment);
			mode.setViewName("redirect:/InfoSpot");	
		}
		
	
		return mode;
	}
	
	
	@RequestMapping(value = "/suppCommentaire", method = RequestMethod.POST)
	public String creation(@RequestParam(required = true) int code,HttpSession session)
	{	
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		
		if (utilisateur != null)
		{
			Commentaire commentaire = commentaireRepository.findOne(new Integer(code));

			if (commentaire != null)
			{
				if (commentaire.getUtilisateur().getUtiCode() == utilisateur.getUtiCode())
				{
					commentaire.setComActif(false);
					commentaireRepository.save(commentaire);
			
					System.out.println("Commentaire supprimer " + code);
				}
			}
		}

		return "";
	}
	
	@RequestMapping(value = "/modifCommentaire", method = RequestMethod.POST)
	public String creation(@RequestParam(required = true) int code,@RequestParam(required = true) String contenu,HttpSession session)
	{	
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		
		if (utilisateur != null)
		{
			Commentaire commentaire = commentaireRepository.findOne(new Integer(code));

			if (commentaire != null)
			{
				if (commentaire.getUtilisateur().getUtiCode() == utilisateur.getUtiCode())
				{
					commentaire.setComContenu(contenu);
					commentaireRepository.save(commentaire);
			
					System.out.println("Commentaire modifier " + code);
				}
			}
		}

		return "";
	}
	

	
		
}
