package com.urban.web;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.urban.entities.Utilisateur;

@Controller
public class AccueilController {
	
	
	
	@RequestMapping(value ="/accueil",  method = RequestMethod.GET)
	public String index(Model model,HttpSession session) {
		
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		

		model.addAttribute("sesssionUtilisateur", utilisateur);
		
		return "WEB-INF/accueil.jsp";
	} 

}
