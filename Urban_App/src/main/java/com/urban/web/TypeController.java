package com.urban.web;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.urban.DAO.LieuRepository;
import com.urban.DAO.TypeRepository;
import com.urban.entities.Lieu;
import com.urban.entities.Type;
import com.urban.entities.Utilisateur;


@Controller
public class TypeController {
	private final String URL = "WEB-INF/consultationTypes.jsp";
	
	@Autowired
	private LieuRepository lieuRepository;
	@Autowired
	private TypeRepository typeRepository;
	
	private final String CLEAPI = "\"AIzaSyC8wT9aShoX-xe7n-eN6loHfhTsqN5NIgU\"";

	@RequestMapping(value = "/Potagers", method = RequestMethod.GET)
	public String potagers(Model model,HttpSession session) {
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		
		model.addAttribute("Text", "Que vous soyez un jardinier néophyte ou expérimenté, les potagers collaboratifs vous tendent les "
				+ "bras ! Leur principe est simple : cultiver des fruits et légumes en ville. Simple parcelle de terre située au sein "
				+ "d\'un parc et ouverte à tous ou véritable jardin partagé nécessitant une inscription, leur but est de redonner aux "
				+ "citadins le contrôle sur leur production alimentaire. Découvrez-les sur notre carte collaborative !");
		
		Type type = typeRepository.getOne(new Integer(4));
		
		model.addAttribute("Marqueurs", rechercheMarqueurs(type));
				
		model.addAttribute("CleApi",CLEAPI);
		
		model.addAttribute("sesssionUtilisateur", utilisateur);
		
		return URL;
	}

	@RequestMapping(value = "/Eaux", method = RequestMethod.GET)
	public String eaux(Model model,HttpSession session) {
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		
		model.addAttribute("Text", "Une petite soif alors que vous êtes en ville ? Pas d\'inquiétudes ! Ouvrez notre carte collaborative "
				+ "et repérez un point d\'eau potable en toute simplicité. Fontaines publiques situées au détour d\'une rue ou dans un parc, "
				+ "robinet de distribution, source d\'eau potable naturelle… De quoi vous réhydrater aisément où que vous soyez !");
		
		Type type = typeRepository.getOne(new Integer(3));
		
		model.addAttribute("Marqueurs", rechercheMarqueurs(type));
		
		model.addAttribute("CleApi",CLEAPI);
		
		model.addAttribute("sesssionUtilisateur", utilisateur);
		
		return URL;
	} 
	
	@RequestMapping(value ="/Sauvages",  method = RequestMethod.GET)

	public String sauvages(Model model,HttpSession session) {
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		
		model.addAttribute("Text", "Symboles du « freeganisme » (mode de vie consistant à consommer de la nourriture d\'origine végétale et "
				+ "gratuite), de nombreux fruits sauvages sont disponibles en ville à différentes saisons. Arbres fruitiers se trouvant sur la "
				+ "voie publique, implantations situées en forêt… Chaque jour, la communauté d\'UrbanFood répertorie sur notre carte collaborative "
				+ "les meilleurs spots de cueillette, pour votre plus grand régal !");
		
		Type type = typeRepository.getOne(new Integer(1));
		
		model.addAttribute("Marqueurs", rechercheMarqueurs(type));
		
		model.addAttribute("CleApi",CLEAPI);
		
		model.addAttribute("sesssionUtilisateur", utilisateur);
		
		return URL;
	} 

	@RequestMapping(value = "/Invendus", method = RequestMethod.GET)
	public String invendus(Model model,HttpSession session) {
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		
		model.addAttribute("Text", "Les invendus alimentaires sont le résultat d\'un phénomène de gaspillage alimentaire (fait de jeter "
				+ "des aliments encore comestibles) aujourd\'hui largement répandu dans notre société. Les membres de la communauté "
				+ "d\'UrbanFood explorent les moyens de lutter contre ce fléau écologique et éthique en ville : boulangeries, "
				+ "restaurants, poubelles de supermarchés… Tous les spots anti-gaspi\' sont à retrouver sur notre carte collaborative !");
		
		Type type = typeRepository.getOne(new Integer(2));
		
		model.addAttribute("Marqueurs", rechercheMarqueurs(type));
		
		model.addAttribute("CleApi",CLEAPI);
		
		model.addAttribute("sesssionUtilisateur", utilisateur);
		
		return URL;
	} 
	
	private String rechercheMarqueurs(Type type) 
	{ 
		List<Lieu> lieux = null;
				
		if (type == null)
			lieux = lieuRepository.findAll();
		else
			lieux = lieuRepository.findByType(type);
		
		System.out.println("taille :" + lieux.size());
		
		StringBuffer chaineRetour = new StringBuffer();
		
		chaineRetour.append("[");
		
		boolean premier = true;
		
		for (Lieu item : lieux)
		{
			if (!premier)
				chaineRetour.append(",");
			
			chaineRetour.append("[\"" + item.getLieNom() + "\"," + item.getLieLatitude().toString() + "," + item.getLieLongitude().toString() 
					+ ",\"" + item.getLieDescription()  + "\"," + Integer.toString(item.getLieCode()) + "]");
			
			premier = false;
		}
		
		chaineRetour.append("]");
		
		
		return chaineRetour.toString();
	}
	
	@RequestMapping(value = "/Carte", method = RequestMethod.GET)
	public String carte(Model model,HttpSession session) {
		
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		
		model.addAttribute("Text", "");
		model.addAttribute("Marqueurs", rechercheMarqueurs(null));
		
		model.addAttribute("CleApi",CLEAPI);
		
		model.addAttribute("sesssionUtilisateur", utilisateur);
		
		return URL;
	} 
}
