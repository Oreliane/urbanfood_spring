package com.urban.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.urban.entities.Utilisateur;
import com.urban.sessions.SessionUtilisateur;

@Controller
public class testController {
	
	
	@Autowired
	private SessionUtilisateur session;
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String carte(Model model) {
		
		model.addAttribute("donnee",session.debug);	
		
		System.out.println("passe ++++++++++++++++");
		session.profile = new Utilisateur();
		System.out.println(session.debug);
		
		return "WEB-INF/test.jsp";
	} 

}
