package com.urban.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.urban.DAO.LieuRepository;
import com.urban.DAO.TypeRepository;
import com.urban.entities.Lieu;
import com.urban.entities.Type;

@RestController
public class TypeControllerRest 
{
	@Autowired
	private LieuRepository lieuRepository;
	@Autowired
	private TypeRepository typeRepository;
	
	
	// Pour test :       http://localhost:8080/UrbanFood/marqueursType?codeType=1
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value = "/marqueursType")
	public ResponseEntity createCustomer(@RequestParam(required = true) int codeType) 
	{

		List<Lieu> lieux = null;
		
		if (codeType == 0)
			lieux = lieuRepository.findAll();
		else
		{
			Type type = typeRepository.findOne(new Integer(codeType));
			
			if (type == null)
				return new ResponseEntity(null, HttpStatus.NOT_FOUND);
			
			lieux = lieuRepository.findByType(type);
		}
		
		List<Coordonnee> coordonnees = new ArrayList<Coordonnee>();
		
		for (Lieu item : lieux)
		{
			coordonnees.add(new Coordonnee(item.getLieCode(),item.getLieNom() ,item.getLieLatitude().toString(),item.getLieLongitude().toString(),item.getLieDescription()));
		}

		return new ResponseEntity(coordonnees, HttpStatus.OK);
	}

}
