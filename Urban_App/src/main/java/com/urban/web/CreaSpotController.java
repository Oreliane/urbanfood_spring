package com.urban.web;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.urban.DAO.LieuRepository;
import com.urban.DAO.TypeRepository;
import com.urban.DAO.VilleRepository;
import com.urban.entities.Commentaire;
import com.urban.entities.Lieu;
import com.urban.entities.Type;
import com.urban.entities.Utilisateur;
import com.urban.entities.Ville;

@Controller
public class CreaSpotController {
	
	@Autowired
	private TypeRepository typeRepository;
	
	@Autowired
	private VilleRepository villeRepository;
	
	@Autowired
	private LieuRepository lieuRepository;
	
	@RequestMapping(value ="/AjoutSpot",  method = RequestMethod.GET)
	public String creation(@RequestParam String lat, @RequestParam String lng, Model model, HttpSession session) {	
		
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		
		List<Type> types = typeRepository.findAll();
		List<Ville> villes = villeRepository.findAll();
		
		
		model.addAttribute("types",types);
		model.addAttribute("villes",villes);
		model.addAttribute("lat",lat);
		model.addAttribute("lng",lng);	
		
		model.addAttribute("sesssionUtilisateur", utilisateur);
		
		return "WEB-INF/creationSpot.jsp";
	}
	
	@RequestMapping(value ="/AjoutSpot",  method = RequestMethod.POST)
	public ModelAndView creation2(String nom, BigDecimal lat, BigDecimal lng, int typeSpot, String description, int ville, ModelAndView model, HttpSession session) {		
		
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		Ville villeObj = villeRepository.findOne(ville);
		Type typeObj = typeRepository.findOne(typeSpot);
		List<Commentaire> comments = new ArrayList<Commentaire>();
		int id = 0;
		
		if (!utilisateur.getUtiNom().isEmpty()) {
		Timestamp date = new Timestamp(System.currentTimeMillis());
		Lieu ajoutLieu = new Lieu(date, description, lat, lng, nom, comments, typeObj, utilisateur, villeObj);
		lieuRepository.save(ajoutLieu);
		
		id = ajoutLieu.getLieCode();
		}
		
		model.addObject("id",id);
		model.addObject("sesssionUtilisateur", utilisateur);
		
		model.setViewName("redirect:/InfoSpot");
		return model;

	}

}
