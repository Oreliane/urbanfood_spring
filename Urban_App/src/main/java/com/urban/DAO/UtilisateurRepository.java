package com.urban.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.urban.entities.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Integer> {
	
	@Query("SELECT utilisateur FROM Utilisateur utilisateur WHERE utilisateur.utiEmail=?1 AND utilisateur.utiMdp=?2 ")
	Utilisateur findBySession (String email, String mdp);

}
