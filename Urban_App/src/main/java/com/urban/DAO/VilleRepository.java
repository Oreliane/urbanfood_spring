package com.urban.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.urban.entities.Ville;

public interface VilleRepository extends JpaRepository<Ville, Integer> {

}
