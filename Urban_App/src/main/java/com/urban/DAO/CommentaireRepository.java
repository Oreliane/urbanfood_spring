package com.urban.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.urban.entities.Commentaire;
import com.urban.entities.Lieu;

public interface CommentaireRepository extends JpaRepository<Commentaire, Integer> {
	
	@Query("SELECT commentaire FROM Commentaire commentaire WHERE commentaire.lieu=?1 ORDER BY commentaire.comCode DESC")
	List<Commentaire> findByLieu (Lieu code);
	
	@Query("SELECT commentaire FROM Commentaire commentaire WHERE commentaire.lieu=?1 and commentaire.comActif=?2 ORDER BY commentaire.comCode DESC")
	List<Commentaire> findByLieu (Lieu code,boolean actif);
	
}
 