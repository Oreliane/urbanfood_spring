package com.urban.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.urban.entities.Type;

public interface TypeRepository extends JpaRepository<Type, Integer> {

}
