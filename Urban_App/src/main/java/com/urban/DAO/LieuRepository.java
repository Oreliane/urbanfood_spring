package com.urban.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.urban.entities.Lieu;

public interface LieuRepository extends JpaRepository<Lieu, Integer> {

	@Query("SELECT lieu FROM Lieu lieu WHERE lieu.type=?1")
	List<Lieu> findByType(com.urban.entities.Type type);

}
