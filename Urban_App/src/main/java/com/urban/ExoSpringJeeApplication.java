package com.urban;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ExoSpringJeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExoSpringJeeApplication.class, args);

	
	}
}
