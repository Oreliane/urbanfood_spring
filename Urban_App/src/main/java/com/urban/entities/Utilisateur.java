package com.urban.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Utilisateur implements Serializable {
	
	@Id
	@GeneratedValue
	@Column(name="UTI_CODE")
	private int utiCode;

	@Column(name="UTI_DATECREA")
	private Timestamp utiDatecrea;

	@Column(name="UTI_EMAIL")
	private String utiEmail;

	@Column(name="UTI_MDP")
	private String utiMdp;

	@Column(name="UTI_NOM")
	private String utiNom;

	//bi-directional many-to-one association to Commentaire
	@OneToMany(mappedBy="utilisateur")
	private List<Commentaire> commentaires;

	//bi-directional many-to-one association to Lieux
	@OneToMany(mappedBy="utilisateur")
	private List<Lieu> lieux;

	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Utilisateur(Timestamp utiDatecrea, String utiEmail, String utiMdp, String utiNom,
			List<Commentaire> commentaires, List<Lieu> lieux) {
		super();
		this.utiDatecrea = utiDatecrea;
		this.utiEmail = utiEmail;
		this.utiMdp = utiMdp;
		this.utiNom = utiNom;
		this.commentaires = commentaires;
		this.lieux = lieux;
	}

	public long getUtiCode() {
		return utiCode;
	}

	public void setUtiCode(int utiCode) {
		this.utiCode = utiCode;
	}

	public Timestamp getUtiDatecrea() {
		return utiDatecrea;
	}

	public void setUtiDatecrea(Timestamp utiDatecrea) {
		this.utiDatecrea = utiDatecrea;
	}

	public String getUtiEmail() {
		return utiEmail;
	}

	public void setUtiEmail(String utiEmail) {
		this.utiEmail = utiEmail;
	}

	public String getUtiMdp() {
		return utiMdp;
	}

	public void setUtiMdp(String utiMdp) {
		this.utiMdp = utiMdp;
	}

	public String getUtiNom() {
		return utiNom;
	}

	public void setUtiNom(String utiNom) {
		this.utiNom = utiNom;
	}

	public List<Commentaire> getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

	public List<Lieu> getLieux() {
		return lieux;
	}

	public void setLieux(List<Lieu> lieux) {
		this.lieux = lieux;
	}
	
	

}
