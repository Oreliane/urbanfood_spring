package com.urban.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Digits;

@Entity
public class Lieu implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="LIE_CODE")
	private int lieCode;

	@Column(name="LIE_DATECREA")
	private Timestamp lieDatecrea;

	@Lob
	@Column(name="LIE_DESCRIPTION")
	private String lieDescription;

	@Column(name="LIE_LATITUDE")
	@Digits(integer = 9 /*precision*/, fraction = 6 /*scale*/)
	private BigDecimal lieLatitude;

	@Column(name="LIE_LONGITUDE")
	@Digits(integer = 9 /*precision*/, fraction = 6 /*scale*/)
	private BigDecimal lieLongitude;

	@Column(name="LIE_NOM")
	private String lieNom;

	//bi-directional many-to-one association to Commentaire
	@OneToMany(mappedBy="lieu")
	private List<Commentaire> commentaires;

	//bi-directional many-to-one association to Type
	@ManyToOne
	@JoinColumn(name="TYP_CODE")
	private Type type;

	//bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name="UTI_CODE")
	private Utilisateur utilisateur;

	//bi-directional many-to-one association to Ville
	@ManyToOne
	@JoinColumn(name="VIL_CODE")
	private Ville ville;

	public Lieu() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Lieu(Timestamp lieDatecrea, String lieDescription, BigDecimal lieLatitude, BigDecimal lieLongitude,
			String lieNom, List<Commentaire> commentaires, Type type, Utilisateur utilisateur, Ville ville) {
		super();
		this.lieDatecrea = lieDatecrea;
		this.lieDescription = lieDescription;
		this.lieLatitude = lieLatitude;
		this.lieLongitude = lieLongitude;
		this.lieNom = lieNom;
		this.commentaires = commentaires;
		this.type = type;
		this.utilisateur = utilisateur;
		this.ville = ville;
	}

	public int getLieCode() {
		return lieCode;
	}

	public void setLieCode(int lieCode) {
		this.lieCode = lieCode;
	}

	public Timestamp getLieDatecrea() {
		return lieDatecrea;
	}

	public void setLieDatecrea(Timestamp lieDatecrea) {
		this.lieDatecrea = lieDatecrea;
	}

	public String getLieDescription() {
		return lieDescription;
	}

	public void setLieDescription(String lieDescription) {
		this.lieDescription = lieDescription;
	}

	public BigDecimal getLieLatitude() {
		return lieLatitude;
	}

	public void setLieLatitude(BigDecimal lieLatitude) {
		this.lieLatitude = lieLatitude;
	}

	public BigDecimal getLieLongitude() {
		return lieLongitude;
	}

	public void setLieLongitude(BigDecimal lieLongitude) {
		this.lieLongitude = lieLongitude;
	}

	public String getLieNom() {
		return lieNom;
	}

	public void setLieNom(String lieNom) {
		this.lieNom = lieNom;
	}

	public List<Commentaire> getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}
		
}
