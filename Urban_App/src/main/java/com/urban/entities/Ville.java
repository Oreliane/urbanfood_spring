package com.urban.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;



@Entity
public class Ville implements Serializable {
	@Id
	@GeneratedValue
	@Column(name="VIL_CODE")
	private int vilCode;

	@Column(name="VIL_NOM")
	private String vilNom;

	//bi-directional many-to-one association to Lieux
	@OneToMany(mappedBy="ville")
	private List<Lieu> lieux;

	public Ville() {
	}

	public Ville(String vilNom) {
		this.vilNom = vilNom;
	}
	
	public Ville(String vilNom, List<Lieu> lieux) {
		this.vilNom = vilNom;
		this.lieux = lieux;
	}

	public int getVilCode() {
		return vilCode;
	}

	public void setVilCode(int vilCode) {
		this.vilCode = vilCode;
	}

	public String getVilNom() {
		return vilNom;
	}

	public void setVilNom(String vilNom) {
		this.vilNom = vilNom;
	}

	public List<Lieu> getLieux() {
		return lieux;
	}

	public void setLieux(List<Lieu> lieux) {
		this.lieux = lieux;
	}
	
	
	
}
