package com.urban.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Type implements Serializable {
	@Id
	@GeneratedValue
	@Column(name="TYP_CODE")
	private int typCode;

	@Column(name="TYP_NOM")
	private String typNom;

	//bi-directional many-to-one association to Lieux
	@OneToMany(mappedBy="type")
	private List<Lieu> lieux;

	public Type() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Type(String typNom, List<Lieu> lieux) {
		super();
		this.typNom = typNom;
		this.lieux = lieux;
	}

	public int getTypCode() {
		return typCode;
	}

	public void setTypCode(int typCode) {
		this.typCode = typCode;
	}

	public String getTypNom() {
		return typNom;
	}

	public void setTypNom(String typNom) {
		this.typNom = typNom;
	}

	public List<Lieu> getLieux() {
		return lieux;
	}

	public void setLieux(List<Lieu> lieux) {
		this.lieux = lieux;
	}

	


}
