package com.urban.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;


@Entity
public class Commentaire implements Serializable {
	@Id
	@GeneratedValue
	@Column(name="COM_CODE")
	private int comCode;

	@Column(name="COM_ACTIF")
	private boolean comActif;

	@Lob
	@Column(name="COM_CONTENU")
	private String comContenu;

	@Column(name="COM_DATECREA")
	private Timestamp comDatecrea;

	//bi-directional many-to-one association to Lieux
	@ManyToOne
	@JoinColumn(name="LIE_CODE")
	private Lieu lieu;

	//bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name="UTI_CODE")
	private Utilisateur utilisateur;

	public Commentaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Commentaire(boolean comActif, String comContenu, Timestamp comDatecrea, Lieu lieu, Utilisateur utilisateur) {
		super();
		this.comActif = comActif;
		this.comContenu = comContenu;
		this.comDatecrea = comDatecrea;
		this.lieu = lieu;
		this.utilisateur = utilisateur;
	}

	public int getComCode() {
		return comCode;
	}

	public void setComCode(int comCode) {
		this.comCode = comCode;
	}

	public boolean isComActif() {
		return comActif;
	}

	public void setComActif(boolean comActif) {
		this.comActif = comActif;
	}

	public String getComContenu() {
		return comContenu;
	}

	public void setComContenu(String comContenu) {
		this.comContenu = comContenu;
	}

	public Timestamp getComDatecrea() {
		
		if (comDatecrea != null)
		{		
			return new Timestamp(comDatecrea.getTime()) { 
				
				@Override
				public String toString() 
				{
					return new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm",java.util.Locale.FRANCE).format(getTime());
				}
			};
		}
		else
			return comDatecrea;
		
	}

	public void setComDatecrea(Timestamp comDatecrea) {
		this.comDatecrea = comDatecrea;
	}

	public Lieu getLieu() {
		return lieu;
	}

	public void setLieu(Lieu lieu) {
		this.lieu = lieu;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	
	
}
