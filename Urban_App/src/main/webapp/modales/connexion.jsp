<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<form id="formConnexion" action="connexion" method="POST">
		<div class="form-group"  id="erreurConnexion">
			
		</div>
		<div class="form-group">
			<label class="control-label labelConnexion">Adresse e-mail :</label>
		</div>
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
			<input name="email" class="form-control" id="email" value="admin@admin.fr" type="text" placeholder="Entrer votre email">
		</div>

		<div class="form-group">
			<label class="control-label labelConnexion">Mot de passe :</label>
			<!--<button type="button" class="btn btn-link">Mot de passe oubli�</button>-->
		</div>
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
			<input name="mdp" class="form-control" id="mdp" type="password" value="admin" placeholder="Entrer votre mot de passe">
		</div>
		<div class="form-group">
		<button type="submit" class="btn btn-primary btn-lg btn-block" id="btnConnexion">Connexion</button>
			<button type="button" class="btn btn-secondary btn-lg btn-block" id="btnInscription">Cr�er votre compte</button>
			
		</div>

	</form>

<script>


										$('#btnInscription').on('click', function() {
											var entete = "<h4 class='modal-title'>Cr�er un compte</h4>";

											var pied = "<button type='submit' class='btn btn-primary'>Valider</button>"
        											+ "<button type='button' class='btn btn-default' data-dismiss='modal'>Annuler</button>";

											
											$('#corpsDial').load('./modales/inscription.jsp');

											$('#enteteDial').html(entete);
											$('#piedDial').html(pied);
										});
</script>
