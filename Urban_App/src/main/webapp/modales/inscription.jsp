<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<form id="formInscription" action="uticreat" method="POST">
		<div class="form-group">
			<label class="cols-sm-2 control-label" for="nom">Nom</label>

			<div class="cols-sm-10">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
					<input name="nom" class="form-control" id="nom" type="text" placeholder="Entrer votre nom" required>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="cols-sm-2 control-label" for="email">Votre E-mail</label>
			<div class="cols-sm-10">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
					<input name="email" class="form-control" id="email" type="text" placeholder="Entrer votre email" required pattern="^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-z]{2,}$">
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="cols-sm-2 control-label" for="mdp">votre mot de passe</label>
			<div class="cols-sm-10">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
					<input name="mdp" class="form-control" id="mdp" type="password" placeholder="Entrer un mot de passe" required>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="cols-sm-2 control-label" for="mdp">Confirmer votre mot de passe</label>
			<div class="cols-sm-10">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
					<input name="mpdConfirme" class="form-control" id="mpdConfirme" type="password" placeholder="Confirmer votre mot de passe" required>
				</div>
			</div>
		</div>
		<div class="form-group">
			<button type='submit' class='btn btn-primary'>Valider</button>
		</div>
		
		<div id="afficherErreur">

		</div>
	</form>


<script>
$('#formInscription').submit(function(event) {

										var erreur = false;
										var texteErreur = '';

										if($('#nom').val() ='')
										{
											erreur = true;
											texteErreur += '<li>Nom vide</li>';
										}


										if ($('#email').val() ='')
										{
											erreur = true;
											texteErreur += '<li>Email vide</li>';
										}


										if($('#mpdConfirme').val() != $('#mpd').val())
										{
											erreur = true;
											texteErreur += "<li>Email n'est pas bien saisie</li>";
										}



										if (erreur)
										{
											

											texteErreur = "<p class='texteErreur'><ol>" + texteErreur + "</ol></p>";

											$('#afficherErreur').html(texteErreur);
										}
							});
</script>
	
