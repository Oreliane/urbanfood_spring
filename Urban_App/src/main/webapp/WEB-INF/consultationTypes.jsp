<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Consultation</title>
<jsp:include page="../partiels/header.jsp" />

<link rel="stylesheet" type="text/css" href="css/hover-min.css">
<script>
	
	 $(function() {
         $('#texte-filtres').click(function(){
        	 
        	 if($('#texte-filtres').html() == '<p class="text-center">Afficher <br> les filtres</p>') {
        		 $('.icones-cachees').css('visibility','visible');
        		 $('#texte-filtres').html('<p class="text-center">Fermer <br> les filtres</p>');
        	 	
        	 }
        	 else {
        		 $('.icones-cachees').css('visibility','hidden');
        		 $('#texte-filtres').html('<p class="text-center">Afficher <br> les filtres</p>');
        	 	
        	 }

         })
     });
	
	
	</script>
<script>
	
	function ajouter(){
			var infowindow = new google.maps.InfoWindow();
		
			google.maps.event.addListener(map, 'click', function (event) {
				var markerAjout = new google.maps.Marker({
					map: map,
					position: event.latLng
				});
				var lat = markerAjout.position.lat().toFixed(6);
				var lng = markerAjout.position.lng().toFixed(6);
				infowindow.setContent("<div class='infobulle'> Cr�er un nouveau spot ? <br /> <a href='./AjoutSpot?lat=" + lat + "&lng=" + lng + "'> OK </a> </div>");
				infowindow.open(map, markerAjout);
			});
	}

	
	var map;
	
	function initMap(locations) {


		//var locations = ${Marqueurs};

		
		map = new google.maps.Map(document.getElementById('map'), {
			center : {
				lat : 47.3898,
				lng : 0.6974
			},
			zoom : 13

		});

		var infowindow = new google.maps.InfoWindow();
		
		var marker, i;

		for (i = 0; i < locations.length; i++) {  
		        marker = new google.maps.Marker({
		        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		        map: map,
		        title: locations[i][0],
		        snippet: locations[i][3],
		           
		      });
				marker.myId = locations[i][4];
		      //marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

		      google.maps.event.addListener(marker, 'click', (function(marker, i) {
		        return function() {
		        	infowindow.setContent("<div class='infobulle'> <h2>" + marker.title +"</h2>" + marker.snippet + " <br /> <a href='./InfoSpot?id=" + marker.myId + "'> Plus d'info </a> </div>");
		        	infowindow.open(map, marker);
		        }
		      })(marker, i));
		    }

		
		//map.addListener('center_changed', function() {
        // window.setTimeout(function() {
        //    map.panTo(marker.getPosition());
        //  }, 3000);
        //});

        //marker.addListener('click', function() {
        //  map.setZoom(15);
        //  map.setCenter(marker.getPosition());
        //});

	}

	function ChargementPage()
	{
		var locations = ${Marqueurs};
		initMap(locations);
	}

	function rechargeMap(code){


		$.ajax({
		    url: 'marqueursType',
		    type: 'GET',
		    data: {codeType:code},
		    contentType: 'application/json; charset=utf-8',
		    dataType: 'json',
		    async: false,
		    success: function(data) {
		    	var tab = [];
		    	
		    	  $.each( data, function(ligne,valLigne) {
			    	 
		    		 var items = []; 
    	 
			    	items.push(valLigne.libelle);
			    	items.push(valLigne.latitude);
			    	items.push(valLigne.longitude);
			    	items.push(valLigne.description);
			    	items.push(valLigne.code);

			    	tab.push(items);
		    	  });

		    	  initMap(tab);
		    }
		});
	}

</script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2WSfYjkrpGqmWCab4IienogIkSQ3IejI&callback=ChargementPage"
	async defer></script>

</head>


<body class="bodyConsultationTypes">




<!-- MENU -->



<jsp:include page="../menu.jsp" />




<div>
<ol class="breadcrumb">
<li><a href="index.jsp" title="Accueil">Accueil</a></li>
<li class="active" id="filArianeActif"></li>
</ol>
</div>

<div class="row">
  <div class="col-xs-4">
  	<div style="text-align: center;">
  	</div>
  </div>
  	<div class="col-xs-5 col-xs-offset-2">
  		<h3></h3>
  			<p style="color:white; margin-bottom: 5%; text-align:right;">
  				${Text}
  			</p>
 
	</div>
</div>
	
	
		<!--------------- AFFICHAGE ICONES NAVIGATION ----------------- -->
	
	       
                
                    
         <div class="row myRow">
                	
                	<div class="col-md-offset-1 col-md-1 hvr-pulse-grow">
                		                	<c:choose>
						<c:when test="${sesssionUtilisateur != null}">
							<img src="img/plus.png" onclick="ajouter()" class="img-responsive" id="icone-plus">
						</c:when>
						<c:otherwise>
							<img src="img/plus.png" onclick="ajouter()" class="img-responsive bloqueIcone" id="icone-plus" title="Merci de vous connectez.">
						</c:otherwise>
					</c:choose>
                	</div>
                	
                	<div class="col-md-offset-1 col-md-1 panel panel-default" id="texte-filtres">
                		<p class="text-center">Afficher <br> les filtres</p>
                	</div>
            
            		<div class="col-md-5 col-md-offset-2 icones-cachees">
	              	
	              		<a href="#" onclick="rechargeMap(1)"><img src="img/sauvages.png" class="img-responsive hvr-pulse-grow icones-type"></a>
	              		<a href="#" onclick="rechargeMap(4)"><img src="img/potagers.png" class="img-responsive hvr-pulse-grow icones-type"></a>
	              		<a href="#" onclick="rechargeMap(3)"><img src="img/eau.png" class="img-responsive hvr-pulse-grow icones-type"></a>
	              		<a href="#" onclick="rechargeMap(2)"><img src="img/invendus.png" class="img-responsive hvr-pulse-grow icones-type"></a>
	              		
	          
	              	</div>
                
         </div>
                
       <!-- Icones qui apparaissent quand on clique sur "montrer les filtres -->
                
<!--             <div class="icones-cachees"  >    
	              <div class="row" id="row-first">  	              
	              </div>

            </div> -->


		<!--------------- AFFICHAGE CARTE ----------------- -->
		<div id="map"> Plop </div>
			

	<jsp:include page="../footer.jsp" />


</body>
</html>