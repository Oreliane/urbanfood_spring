<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page import="java.util.*" %>

<!DOCTYPE html>
<html>
<head>
<title>UrbanFood</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="../partiels/header.jsp" />

<script src="js/main.js" type="text/javascript"></script>
<script type="text/javascript">
	/****** Pas de défilement automatique du carousel et réaction au passage de souris ******/
	$(document).ready(function() {
		$('.carousel').carousel({
			pause : true,
			interval : false
		});

		$(".carousel").mouseenter(function() {
			$(this).carousel(1);
		});

		$(".carousel").mouseleave(function() {
			$(this).carousel(0);
		});
	});
</script>

</head>
<body>
<jsp:include page="../menu.jsp" />

	<h1>Urban Food  </h1>
	
	<div class="container-fluid" id="fond">
		<div class="container" id="c1">
			<div class="row">
				<div class="col-sm-6 col-lg-3">
					<div class="carousel slide">
						<div class="carousel-inner">
							<div class="item active">
								<a href='./Sauvages'> <img class="img-responsive"
									src="img/sauvages.png" alt="" /></a>
							</div>
							<div class="item ">
								<a href='./Sauvages'> <img class="img-responsive"
									src="img/txtSauvages.png" alt="Glanages Sauvages" /></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="carousel slide">
						<div class="carousel-inner">
							<div class="item active">
								<a href='./Potagers'> <img class="img-responsive"
									src="img/potagers.png" alt="Potagers Collectifs" /></a>
							</div>
							<div class="item ">
								<a href='./Potagers'> <img class="img-responsive"
									src="img/txtPotagers.png" alt="" /></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3 ">
					<div class="carousel slide">
						<div class="carousel-inner">
							<div class="item active">
								<a href='./Eaux'> <img class="img-responsive" src="img/eau.png"
									alt="" /></a>
							</div>
							<div class="item ">
								<a href='./Eaux'> <img class="img-responsive"
									src="img/txtEau.png" alt="Points d'eau potable" /></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3 ">
					<div class="carousel slide">
						<div class="carousel-inner">
							<div class="item active">
								<a href='./Invendus'> <img class="img-responsive"
									src="img/invendus.png" alt="" /></a>
							</div>
							<div class="item ">
								<a href='./Invendus'> <img class="img-responsive"
									src="img/txtInvendus.png" alt="Invendus" /></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container" id="c2">
			<div class="row">
				<div class="col-sm-6 col-md-12">
					<div class="carousel slide">
						<div class="carousel-inner">
							<div class="item active">
								<a href='./Carte'> <img class="img-responsive" src="img/carte.png"
									alt="" />
								</a>
							</div>
							<div class="item ">
								<a href='./Carte'> <img class="img-responsive"
									src="img/txtCarte.png" alt="Carte des spots" /></a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<jsp:include page="../footer.jsp" />

</body>

</html>
