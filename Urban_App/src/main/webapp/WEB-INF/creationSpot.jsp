<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html class="bodyConsultationSpot">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
<title>Ajout d'un nouveau spot</title>
<jsp:include page="../partiels/header.jsp" />

<script>
	var map;
	function initMap() {
		var lieLatitude = ${lat};
		var lieLongitude = ${lng};

		map = new google.maps.Map(document.getElementById('mapLieu'), {

			center : {
				lat : lieLatitude,
				lng : lieLongitude
			},
			zoom : 13
		});

		var infowindow = new google.maps.InfoWindow();

		var marker, i;

		marker = new google.maps.Marker({
			position : new google.maps.LatLng(lieLatitude, lieLongitude),
			map : map,
		});

	}
</script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2WSfYjkrpGqmWCab4IienogIkSQ3IejI&callback=initMap"
	async defer></script>
</head>
<body>
	<jsp:include page="../menu.jsp" />

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 col-md-offset-1 ">

				<div id="mapLieu" class="lieu "></div>

			</div>
			<div class="col-md-6 vert">
				<form id="form" action="AjoutSpot" method="post">
					<div class="input-group">
						<span class="input-group-addon">Nom du spot</span> <input type="text"
							class="form-control" name="nom" id="nom" required>
					</div>
					<br>
				<div class="input-group">
						<span class="input-group-addon">Latitude</span> <input type="text"
							class="form-control" name="lat" id="lat" value="${lat}" readonly>
					</div>
					
					<div class="input-group">
						<span class="input-group-addon">Longitude</span> <input
							type="text" class="form-control" name="lng" id="lng"
							value="${lng}" readonly>
					</div>
					
					<div class="input-group">
						<span class="input-group-addon">Ville</span> 
						<select class="form-control" id="ville" name=ville required>
							<option></option>
							<c:forEach items="${villes}" var="villes">
								<option value="${villes.vilCode}"> ${villes.vilNom}</option>
							</c:forEach>
						</select>
					</div>
					
				
					<div class="input-group">
						<span class="input-group-addon">Type de spot</span> 
						<select	class="form-control" id="typeSpot" name="typeSpot" required>
							<option></option>
							<c:forEach items="${types}" var="types">
								<option value="${types.typCode}">${types.typNom}</option>
							</c:forEach>
						</select>
					</div>
		
			
		<br>
			<div class="form-group">
				<label for="description">Description :</label>
				<textarea class="form-control" rows="5" id="description" name="description" required></textarea>
			</div>
			<button type="submit" class="btn btn-default">Créer le spot</button>
			</form>
		</div>
</div>
</div>
	<jsp:include page="../footer.jsp" />
</body>
</html>