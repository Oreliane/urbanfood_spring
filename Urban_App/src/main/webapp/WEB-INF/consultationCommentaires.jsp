<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html class="bodyConsultationSpot">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Spot</title>
<jsp:include page="../partiels/header.jsp" />
<script>
	var map;
	function initMap() {
		var lieLatitude = ${lieu.lieLatitude};
		var lieLongitude = ${lieu.lieLongitude};
		map = new google.maps.Map(document.getElementById('mapLieu'), {

			center : {
				lat : lieLatitude,
				lng : lieLongitude
			},
			zoom : 13
		});

		var infowindow = new google.maps.InfoWindow();

		var marker, i;

		marker = new google.maps.Marker({
			position : new google.maps.LatLng(lieLatitude, lieLongitude),
			map : map,
		});

	}

	function suppressionCom (idCode) 
	{
		$.post('suppCommentaire',{code: idCode }, function() { window.location.reload(); });
	}

	function modifCom (idCode,nomDiv) 
	{

		var elt = document.getElementById(nomDiv);
		var texteModif = elt.innerText || elt.textContent;
				

		
		$.post('modifCommentaire',{code: idCode,contenu :texteModif  }, function() { window.location.reload(); });
	}
	
</script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2WSfYjkrpGqmWCab4IienogIkSQ3IejI&callback=initMap"
	async defer></script>
</head>
<body>
	<jsp:include page="../menu.jsp" />

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 col-md-offset-1 ">
			<div class="rond"> <h2> ${lieu.lieNom} </h2>  ${lieu.lieDescription} </div>
								<div id="mapLieu" class="lieu "></div>
				<form action="InfoSpot" method="post">
					<div class="form-group vert">
						<label for="comment">Ajouter un commentaire :</label> 
						<input type="text" class="form-control" name="comment" id="comment">
						<input type="hidden" name="id" id="id" value="${id}">
						<button type="submit" class="btn btn-default">Envoyer</button>
					</div>
				</form>
			</div>
						<div class="col-md-6">
				<c:forEach items="${commentaires}" var="comm">
					<div class="vert" >
						<div class="gras" >
			
<c:if test = "${sesssionUtilisateur.utiCode == comm.utilisateur.utiCode}">
        <button class="droite" id="Suppr${comm.comCode}" onClick="suppressionCom(${comm.comCode})">Supprimer</button>
     </c:if>

							
							<c:out value="${comm.utilisateur.utiNom} " />
							
							<c:out value="${comm.comDatecrea} " />
						</div >
						
						
						<c:choose>
								<c:when test="${sesssionUtilisateur.utiCode == comm.utilisateur.utiCode}">
									<div id='divTexte_${comm.comCode}' contenteditable="true" onblur="modifCom(${comm.comCode},'divTexte_${comm.comCode}')">
								</c:when>
								<c:otherwise>
									<div>
								</c:otherwise>
							</c:choose>
						
						
						
						<c:out value="${comm.comContenu} " />
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>

	<jsp:include page="../footer.jsp" />
</body>
</html>