<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script>
	$(document).ready(function() {
		$('[data-toggle="popover"]').popover();
	});
</script>
<title>footer</title>

</head>
<body>
	<footer id="footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">
					&#9400 2017 Urban Food <br>
					<a href="#footer" data-toggle="popover" title="Copyright des images"
						data-content="Icônes faits par Madebyoliver et Freepik (https://www.flaticon.com), licence CC 3.0 BY. Image de fond (www.pexel.com)">Images
						copyright</a>
				</div>
				<div class="col-sm-6">

					<p class="text-right">
						<i class="fa fa-facebook-official fa-2x"></i> 
						<i	class="fa fa-twitter-square fa-2x"></i> 
							<i class="fa fa-google-plus-square fa-2x"></i>
					</p>
				</div>
			</div>

		</div>
	</footer>
</body>
</html>