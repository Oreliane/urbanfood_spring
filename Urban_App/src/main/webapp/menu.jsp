<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!doctype html>
<html lang="en">
    <head>     
        <meta charset="utf-8" />
          
    </head>
    <body class="bodyMenu">
          
		 <nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">UrbanFood</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		      
		      <!-- LIEN --> <li><a href="./accueil">Accueil</a></li>
				
				
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Spots<span class="caret"></span></a>
		          <ul class="dropdown-menu">
		        	   <!-- LIEN --> <li><a href="./Carte">Carte Générale</a></li>
		             <!-- LIEN --> <li><a href="./Sauvages">Glanages sauvages</a></li>
		             <!-- LIEN --> <li><a href="./Potagers">Potagers collectifs</a></li>
		             <!-- LIEN --> <li><a href="./Eaux">Points d'eau potable</a></li>
		             <!-- LIEN --> <li><a href="./Invendus">Invendus</a></li>
		          </ul>
		        </li>
		        <c:choose>
					<c:when test="${sesssionUtilisateur != null}">
					<li><a href="./deconnexion">Se déconnecter <span class="glyphicon glyphicon-off" style="font-size:12px;"></span></a></li>
					</c:when>
					<c:otherwise>
				   <li><a id="btnConnexion" href="#"> Se connecter <span class="glyphicon glyphicon-off" style="font-size:12px;"></span></a></li>
					</c:otherwise>
				</c:choose>    
				</ul>  		        
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>


    </body>
</html>


