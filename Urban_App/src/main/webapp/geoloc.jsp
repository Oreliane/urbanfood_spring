<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      
      #map {
        height: 100%;
      }
     
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 47.3898, lng: 0.6974},
          zoom: 13
        });
		
	    google.maps.event.addListener(map, 'click', function (event) {
			var marker = new google.maps.Marker({
				map: map,
				position: event.latLng
			});
		});
		


		
		map.addListener('center_changed', function() {
          window.setTimeout(function() {
            map.panTo(marker.getPosition());
          }, 3000);
        });

        marker.addListener('click', function() {
          map.setZoom(15);
          map.setCenter(marker.getPosition());
        });

	}
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2WSfYjkrpGqmWCab4IienogIkSQ3IejI&callback=initMap"
    async defer></script>
  </body>
</html>